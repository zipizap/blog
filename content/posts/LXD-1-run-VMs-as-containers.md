---
title: "LXD 1 run VMs as containers"
date: 2019-12-16 


draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- 4hackers
#- 4nonhackers
- informative
- lxd
- lxc
#- exploitation
#- suid
#- etc
#- portugues
#- sociology

---
<div>
<div ID="freeplaneNode__ID_423582856" class=FreeplaneNodeGen>
    <p>
      <b><font size="4">Prelude</font></b><br><br>Well well well... this is the opening of a big topic for me... around 2012 I've read through somethings about kernel namespaces and then lxc, and to be honest, at that time all this was still under heavy development and documentation was not very gentle, and I didnt understand what lxc was more than this idea of &quot;having kernel containers&quot;. Well, as time went by my curiosity lead me to the ubuntu LXC and latter LXD project, which added shape and usability to create &quot;light-weight VMs&quot;.<br><br>I've more or less followed LXD since then, and it has been a very greatfull journey :) The LXD dev-team is small but very talented and open, and their project vision kept evolving and resonating with other remarkable open-source innovations that emerged in paralell: golang, snap, zfs, sdn-trends, etc)
    </p>
    <p>
<br>
    </p>
    <p>
      So, I'll try to write down some of my personal notes around LXC/LXD, in a series of blog posts.
    </p>
    <p>
<br>
    </p>
    <p>
      Without further ado - ladies and gentleman I present to you LXC/LXD :)
    </p>
    <p>
      <br>
      <br>
<br>
    </p>
    <p>
      <br>
      <b><font size="4">The LXC/LXD project</font></b><br>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      LXC/LXD (<a href="https://linuxcontainers.org/">Linux Containers</a>) are 2 combined projects that leverages the linux-kernel features to create <b><i>VMs</i></b>-as-containers.
    </p>
    <p>
      <br>
      LXC/LXD in essence is an idea similar to Docker - while Docker creates containers with the philosophy of <b><u>Process</u></b>-as-a-container, the LXC/LXD follows a different philosophy of <b><u>VM</u></b>-as-a-container.<br>So, with LXC/LXD we can create containers-that-behave-like-VMs (with an init-system, multiple processes, local logs, etc...) that.<br>Also, as it happens with docker and other linux-containers technologies, all the containers share the same common and unique <u>linux kernel</u>&#160;from the underlying host.<br><br>
    </p>
    <p>
      There is an online demo where you can try LXD here <a href="https://linuxcontainers.org/lxd/try-it/">https://linuxcontainers.org/lxd/try-it/</a>&#160;<br><br>
    </p>
    <p>
      There are pre-made container <u>images</u>&#160;of popular linux distros, like ubuntu18.04, centos, fedora, alpine, etc, etc...
    </p>
    <p>
      There is also configuration options for managing <u>networks</u>, <u>storage</u>, <u>devices</u>, <u>containers</u>, <u>snapshots</u>, lxc-hosts, clusters-of-lxc-hosts and more...
    </p>
    <p>
<br>
    </p>
    <p>
      Also, LXD in the hosts, can use multiple storage-backends to save the images/containers/snapshots. Notably LXD is aware of specific filesystem-features and takes great advantage from ZFS properties (ZFS, btrfs, etc) to avoid duplicate data and increase efficiency (ex: 2 containers from same image will not use double the space, but barely the same space, as ZFS will only store the incremental differences)
    </p>
    <p>
      <br>
      It also supports lots of options for networking configurations both for intra-host and for inter-hosts lxc-clusters
    </p>
    <p>
      <br>
      So we can use LXC/LXD to create multiple containers, running different Linux OSs, and connect them via multiple network as we want.
    </p>
    <p>
      <br>
      As it happens with Docker containers, by default the RAM and CPU usage is shared between all the containers which allows for higher density of containers running in pseudo-isolated fashion.<br>
    </p>
    <p>
      Do keep in mind that the same linux-kernel of the host is shared by all containers, which implies some security considerations to keep in mind - basically dont think containers are &quot;securely isolated&quot;, they are as secure as docker and all other containers can be with a shared-linux-kernel. That being said, the lxc/lxd developers are keen and participate actively in security improvements around lxc/lxd as kernel features evolve - which gives a sense that security is as best as can be done within containers. So rest assured and just dont forget its not 100% bulletproof. More info see [4], [5], [6].
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      The LXC/LXD development team is small but highly focused and mature, and very friendly-and-responsive via the lxd forums [0]<br>The official documentation manual [1] is not very easy to read, but the blogs of some lxd members [2] [3] provide very good usage examles. Also there are announcements in the forum describing the new features introduced [0]
    </p>
    <p>
<br>
    </p>
    <p>
      The latest version of LXC/LXD is always available via &quot;snap&quot;, and announced with details in the lxd-forum [0].<br>The LXC/LXD &quot;readthedocs&quot; is not very friendly nor descriptive, more like a reference. The best introductions, explanations an examples, are usually found in the devs-blogs [2] [3]. Any more-complex questions or doubts should be directed to the lxd-forum[0]
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      NOTE: *lxd* is like the higher-level tech-stack that uses the lower-level *lxc* tech-stack. In my posts I'll just use LXD and LXC interchangeably. To get into details see [7]
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      [0] <a href="https://discuss.linuxcontainers.org/c/lxd">https://discuss.linuxcontainers.org/c/lxd</a>
    </p>
    <p>
      [1] <a href="https://lxd.readthedocs.io/en/latest/">https://lxd.readthedocs.io/en/latest/</a>
    </p>
    <p>
      [2] <a href="https://stgraber.org/2016/03/11/lxd-2-0-introduction-to-lxd-112/">https://stgraber.org/2016/03/11/lxd-2-0-introduction-to-lxd-112/</a>
    </p>
    <p>
      [3] <a href="https://blog.simos.info/">https://blog.simos.info/</a><br>[4] <a href="https://lwn.net/Articles/796700/">https://lwn.net/Articles/796700/</a>
    </p>
    <p>
      [5] <a href="https://brauner.github.io/2019/02/12/privileged-containers.html">https://brauner.github.io/2019/02/12/privileged-containers.html</a><br>[6] <a href="https://www.youtube.com/watch?v=uSrPrl3tn5U">https://www.youtube.com/watch?v=uSrPrl3tn5U</a><br>[7] <a href="https://discuss.linuxcontainers.org/t/comparing-lxd-vs-lxc/24">https://discuss.linuxcontainers.org/t/comparing-lxd-vs-lxc/24</a>
    </p>
</div>
</div>
