---
title: "Frustration a part of hacking"
date: 2019-06-02 


draft: false

categories: 
- Infosec
#- Devops
#- Sysadmin
#- Cloud
#- Programming

tags:
#- 4hackers
- 4nonhackers
- informative
#- exploitation
#- suid
#- etc

---
<div>
<div ID="freeplaneNode__ID_1719485162" class=FreeplaneNodeGen>
<p>Frustration is a part of hacking - an invisible big part</p></div>
<div ID="freeplaneNode__ID_1005271932" class=FreeplaneNodeGen>
    <p>
      Its part of hacking's nature, to experience frustration most of the time as you are searching mostly difficult and uncertain solutions.&#160;&#160;For each solution found, there have been many intents tried, in the same or different contexts, that were performed with maximum intensity to improve chances for a successfull outcome.&#160;&#160;And most of them fail... untill you finally found one, sometimes sooner, sometimes latter, or much much latter.
    </p>
    <p>
<br>
    </p>
    <p>
      And this seems the normal process: intensely try many times, many difficult things (some if which demand planned study and specialization) keeping to maintain the optimal ratio of minimal-mistakes versus maximum-speedthrough.
    </p>
    <p>
<br>
    </p>
    <p>
      Frustration can sometimes lead to other feelings: stress, anger, self-dis-esteem, depression. So, for long-term hacking its usefull to learn to manage frustration, and other feelings.
    </p>
    <p>
<br>
    </p>
    <p>
      Hacking starts and stops - emotionally put a limit from it and the rest of your life. Give a though to try to introspect and unleash from negative emotions, and focus more on positive emotions.&#160;&#160;Enjoy the &quot;electric pulse&quot; high when you finally overcome a challenge that you have been working on since 4 weeks. Live the good, avoid the bad. The fact that you engage in such a mentally intensive and complex activity, can affect any person in more or less degree, but how you deal (or not) with this, on long term can shape how to take it through your life.
    </p>
    <p>
<br>
    </p>
    <p>
      And this is where humor and motivation comes in - they are the magic chemical that dissolves negative-feelings, and should be taken with good healthy doses in hacking - and should be real, honest, true - you can probably clean much better yourself's emotions, if your core is not locked-down but instead can expand and reach others, and let be reached by others. It will grow your core, you will grow too, and others will grow with you. And you probably need it in good shape, for long-term hacking.&#160;&#160;Also, as a side benefit, you'll live happier anyway :) &#160;
    </p>
    <p>
<br>
    </p>
    <p>
      Thing is - grow this too together with your hacking skills, its not a maybe its a must :)
    </p>
    <p>
<br>
    </p>
    <p>
      Happy hacking
    </p>
    <p>
<br>
    </p>
    <p>
      PS: The &quot;<b><u>hacking&quot;</u></b>&#160;word is nowadays used with different meaning on different contexts. I've accepted this already, and one should be aware of this confusion, to clarify other readers.
    </p>
    <p>
      In the above article, I use the word <u><b>&quot;hacking&quot; </b>as<b>&#160;&quot;the computer-science specialization in security&quot;</b></u>.&#160;&#160;
    </p>
    <p>
      Just that, and nothing about who does it (good/bad), nor what does with it (attack/protect) - as it does not matter in this discussion (its certainly hard whichever side of the fence).
    </p>
</div>
</div>
