---
title: "Bash poetic moments"
date: 2019-12-29

draft: false

categories: 
#- Infosec
#- Devops
- Sysadmin
#- Cloud
#- Programming

tags:
#- 4hackers
- 4nonhackers
- informative
#- exploitation
#- suid
#- etc
#- portugues
#- sociology
- bash

---
<div>
<div ID="freeplaneNode__ID_1439770469" class=FreeplaneNodeGen>
<p>Bash - that everlasting scripting language that you love and hate and love again.</p>
<p>No more talk - lets go straight to the shell</p></div>
<div ID="freeplaneNode__ID_794214718" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Usefull script-template/boilerplate </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_794327695" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>#!/usr/bin/env bash</p>
<p># zipizap</p>
<p>__dir="$(cd "$(dirname "$&lbrace;BASH_SOURCE[0]&rbrace;")" &amp;&amp; pwd)"</p>
<p>__dbg_on_off=on&nbsp;&nbsp;# on off</p>
<p>function shw_info &lbrace; echo -e '\033[1;34m'"$1"'\033[0m'; &rbrace;</p>
<p>function error &lbrace; echo "ERROR in $&lbrace;BASH_LINENO[0]&rbrace; : $@"; exit 1; &rbrace;</p>
<p>function dbg &lbrace; [[ "$__dbg_on_off" == "on" ]] || return; echo -e '\033[1;34m'"dbg $(date +%Y%m%d%H%M%S) $&lbrace;BASH_LINENO[0]&rbrace;\t: $@"'\033[0m';&nbsp;&nbsp;&rbrace;</p>
<p>exec &gt; &gt;(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2&gt;&amp;1</p>
<p>set -o errexit</p>
<p>set -o pipefail</p>
<p>set -o nounset</p>
<p>set -o xtrace</p></div>
</div>
<div ID="freeplaneNode__ID_656222392" class=FreeplaneNodeGen>
<p>And now some of it explained more slowly...</p></div>
<div ID="freeplaneNode__ID_401970871" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>#!/usr/bin/env bash</p>
<p></p>
<p># join stderr&gt;stdout and log them into a file</p>
<p>exec &gt; &gt;(tee -i /tmp/$(date +%Y%m%d%H%M%S.%N)__$(basename $0).log ) 2&gt;&amp;1</p>
<p></p>
<p># debug: show commands (set -x)</p>
<p>set -o xtrace</p>
<p></p>
<p># exit immediately if a simple command exits with a nonzero exit value.</p>
<p># A simple command is any command not part of:</p>
<p>#&nbsp;&nbsp;- *test* in an if, while, or until</p>
<p>#&nbsp;&nbsp;- an &amp;&amp; or || list</p>
<p>#&nbsp;&nbsp;- inside a pipe|line|of|commands and not being the rightmost-last-command</p>
<p>set -o errexit</p>
<p></p>
<p># exit immediately also when command|is|inside|a|pipe|line</p>
<p>set -o pipefail</p>
<p></p>
<p># exit when your script tries to use undeclared variables.</p>
<p>set -o nounset</p></div>
</div>
<div ID="freeplaneNode__ID_1452247938" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><i><font color="#cc99ff">set -o errexit </font></i></b>: Clarifying what/when an exitCode=!0 will interrupt a script, with <b><i><font color="#cc99ff">set -o errexit </font></i></b><br>
    </p>
    <p>
      &#160;&#160;-&#160;&#160;In lines without &amp;&amp; ||, the <u>first failed-command </u>will interrupt the script on the spot
    </p>
    <p>
      &#160;&#160;-&#160;&#160;The &amp;&amp; || &quot;saves&quot; any(all) failed-command(s) in that line&#160;&#160;(in middle of pipes or not)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;Exception: Inside MyVAR=$(expressions variables only || &quot;saves&quot; but &amp;&amp; does not &quot;save&quot;)
    </p>
</div>
<div ID="freeplaneNode__ID_288291682" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Quick-Color functions </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_140769227" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>function shw_grey &lbrace; echo -e '\033[1;30m'"$1"'\033[0m'; &rbrace;</p>
<p>function shw_norm &lbrace; echo&nbsp;&nbsp;&nbsp;&nbsp;"$1"; &rbrace;</p>
<p>function shw_info &lbrace; echo -e '\033[1;34m'"$1"'\033[0m'; &rbrace;</p>
<p>function shw_warn &lbrace; echo -e '\033[1;33m'"$1"'\033[0m'; &rbrace;</p>
<p>function shw_err&nbsp;&nbsp;&lbrace; echo -e '\033[1;31mERROR: '"$1"'\033[0m'; &rbrace;</p></div>
</div>
<div ID="freeplaneNode__ID_907417196" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Read file line by line (without space problems)&#160;</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1380515031" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>while read -u 10 p; do</p>
<p>&nbsp;&nbsp;echo $p</p>
<p>done 10&lt;peptides.txt</p></div>
</div>
<div ID="freeplaneNode__ID_1848231663" class=FreeplaneNodeGen>
    <p>
      <a href="https://stackoverflow.com/a/1521498/259192">https://stackoverflow.com/a/1521498/259192</a>
    </p>
</div>
<div ID="freeplaneNode__ID_541574263" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Bash noop command :</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1491379533" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      :
    </p>
    <p>
<br>
    </p>
    <p>
      # : (a colon) Do nothing beyond expanding arguments and performing redirections. The return status is zero.
    </p>
    <p>
      #
    </p>
    <p>
      # : its like an alias for <u>true</u>
    </p>
    <p>
      # its better to use true as its more readable
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_730739531" class=FreeplaneNodeGen>
    <p>
      <a href="https://stackoverflow.com/a/17583599/259192">https://stackoverflow.com/a/17583599/259192</a>
    </p>
</div>
<div ID="freeplaneNode__ID_322465310" class=FreeplaneNodeGen>
<p>--------</p></div>
<div ID="freeplaneNode__ID_1643615103" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Substitute_BLOCK_with_FILEcontents</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_865566357" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p># This function reads text from stdin, and substitutes a *BLOCK* with the contents from a FILE, and outputs to stdout</p>
<p># The BLOCK is indicated with BLOCK_StartRegexp and BLOCK_EndRegexp</p>
<p>#</p>
<p># Usage:</p>
<p>#&nbsp;&nbsp;&nbsp;&nbsp;seq 100 110 | substitute_BLOCK_with_FILEcontents '^102' '^104' /tmp/FileWithContents &gt; /tmp/result.txt</p>
<p>function substitute_BLOCK_with_FILEcontents &lbrace;</p>
<p>&nbsp;&nbsp;local BLOCK_StartRegexp="$&lbrace;1&rbrace;"</p>
<p>&nbsp;&nbsp;local BLOCK_EndRegexp="$&lbrace;2&rbrace;"</p>
<p>&nbsp;&nbsp;local FILE="$&lbrace;3&rbrace;"</p>
<p>&nbsp;&nbsp;sed -e "/$&lbrace;BLOCK_EndRegexp&rbrace;/a ___tmpMark___" -e "/$&lbrace;BLOCK_StartRegexp&rbrace;/,/$&lbrace;BLOCK_EndRegexp&rbrace;/d" | sed -e "/___tmpMark___/r $&lbrace;FILE&rbrace;" -e '/___tmpMark___/d'</p>
<p>&rbrace;</p></div>
</div>
<div ID="freeplaneNode__ID_980073216" class=FreeplaneNodeGen>
    <p>
      <a href="https://unix.stackexchange.com/a/485670/267617">https://unix.stackexchange.com/a/485670/267617</a>
    </p>
</div>
<div ID="freeplaneNode__ID_945233346" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Show arguments one by one </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1260176304" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>while&nbsp;&nbsp;[[ $# -gt 0 ]]; do</p>
<p>&nbsp;&nbsp;echo "--- $1 ---"</p>
<p>&nbsp;&nbsp;shift</p>
<p>done</p>
<p>exit 99</p></div>
</div>
<div ID="freeplaneNode__ID_1299312445" class=FreeplaneNodeGen>
    <p>
      <a href="https://stackoverflow.com/a/14203146">https://stackoverflow.com/a/14203146</a>
    </p>
</div>
<div ID="freeplaneNode__ID_163259769" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">urldecode </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1977324261" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>urldecode() &lbrace; : "$&lbrace;*//+/ &rbrace;"; echo -e "$&lbrace;_//%/\\x&rbrace;"; &rbrace;</p></div>
</div>
<div ID="freeplaneNode__ID_1206151626" class=FreeplaneNodeGen>
    <p>
      <a href="https://stackoverflow.com/questions/6250698/how-to-decode-url-encoded-string-in-shell">https://stackoverflow.com/questions/6250698/how-to-decode-url-encoded-string-in-shell</a>
    </p>
</div>
<div ID="freeplaneNode__ID_804541212" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Bash connect and write into tcp host/port (not udp, only tcp)</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1221810717" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>echo "hi" &gt;/dev/tcp/127.0.0.1/8377</p></div>
</div>
<div ID="freeplaneNode__ID_1475660000" class=FreeplaneNodeGen>
    <p>
      <a href="https://unix.stackexchange.com/questions/336876/simple-shell-script-to-send-socket-message">https://unix.stackexchange.com/questions/336876/simple-shell-script-to-send-socket-message</a>
    </p>
</div>
<div ID="freeplaneNode__ID_718557854" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Check if there is any local process LISTENing on port 35009tcp </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_330015510" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>ub@ub:~/Tunnels$ lsof -Pi :35009 -sTCP:LISTEN -t</p>
<p>ub@ub:~/Tunnels$ echo $?</p>
<p>1</p>
<p>ub@ub:~/Tunnels$ lsof -Pi :35001 -sTCP:LISTEN -t</p>
<p>2863</p>
<p>ub@ub:~/Tunnels$ echo $?</p>
<p>0</p></div>
</div>
<div ID="freeplaneNode__ID_1703669843" class=FreeplaneNodeGen>
    <p>
      <a href="https://unix.stackexchange.com/a/149478">https://unix.stackexchange.com/a/149478</a>
    </p>
</div>
<div ID="freeplaneNode__ID_476901382" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Check if local-or-remote-port is open </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_266537139" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>( echo -n '' &gt; /dev/tcp/10.11.12.13/12345 ) &amp;&gt;/dev/null &amp;&amp; echo "OK - Port received connection" || echo "NOK - Port did not received connection"</p></div>
</div>
<div ID="freeplaneNode__ID_297712961" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Get ip of interface </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1638669724" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>IFACE=tun0</p>
<p>IP=$(ip -br addr show $&lbrace;IFACE&rbrace;| awk '&lbrace; print $3 &rbrace;' | cut -f1 -d/)</p></div>
</div>
<div ID="freeplaneNode__ID_1752486976" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Timeout </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_349744214" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      # Execute <font color="#ffffff">slow-command</font>
    </p>
    <p>
      #&#160;&#160;- if still running <font color="#ccccff">after 90secs</font>&#160;then <font color="#ccccff">send SIGTERM</font>&#160;(can be trapped)
    </p>
    <p>
      #&#160;&#160;- if still running <font color="#ff9999">after 30secs</font>&#160;then <font color="#ff9999">send SIGKILL</font>&#160;(cannot be trapped)
    </p>
    <p>
      #
    </p>
    <p>
      # '1s' seconds,&#160;&#160;&#160;'2m' minutes,&#160;&#160;&#160;'3h' hours,&#160;&#160;&#160;'4d' days
    </p>
    <p>
      #
    </p>
    <p>
      # exit-code =
    </p>
    <p>
      #&#160;&#160;&#160;&#160;xxx&#160;&#160;&#160;the &quot;natural&quot; command-exit-code, if the command terminates &quot;naturally&quot; without any timeout
    </p>
    <p>
      #&#160;&#160;&#160;&#160;124&#160;&#160;&#160;if command timed-out and was terminated by SIGTERM
    </p>
    <p>
      #&#160;&#160;&#160;&#160;129&#160;&#160;&#160;if command timed-out, then received SIGTERM but did not terminate, then was latter sent SIGKILL and terminated
    </p>
    <p>
<br>
    </p>
    <p>
      timeout <font color="#ff9999">--kill-after=30s</font>&#160;<font color="#ccccff">90s</font>&#160;<font color="#ffffff">/path/to/slow-command arg1 arg2</font>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_411689277" class=FreeplaneNodeGen>
    <a href="https://stackpointer.io/unix/unix-linux-run-command-with-timeout/500/">https://stackpointer.io/unix/unix-linux-run-command-with-timeout/500/</a>
</div>
<div ID="freeplaneNode__ID_945793861" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Newlines in &quot;$VAR&quot; (ok) and $VAR (nok)</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_815346019" class=FreeplaneNodeGen>
<p>Newlines in vars: use "$VAR" never $VAR without double-quotes</p>
<p>Double-quoting "$VAR" shows its newlines (without double-quoting $VAR the newlines get removed)</p></div>
<div ID="freeplaneNode__ID_1478469106" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>root@kali:~/configs/kalisRepo/HTB/machines/Javis# A='a</p>
<p>&gt; b</p>
<p>&gt; c'</p>
<p>root@kali:~/configs/kalisRepo/HTB/machines/Javis# echo "$&lbrace;A&rbrace;"</p>
<p>a</p>
<p>b</p>
<p>c</p>
<p>root@kali:~/configs/kalisRepo/HTB/machines/Javis# echo "$A"</p>
<p>a</p>
<p>b</p>
<p>c</p>
<p>root@kali:~/configs/kalisRepo/HTB/machines/Javis# echo $A</p>
<p>a b c</p>
<p>root@kali:~/configs/kalisRepo/HTB/machines/Javis# echo $&lbrace;A&rbrace;</p>
<p>a b c</p></div>
</div>
<div ID="freeplaneNode__ID_162707493" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Condition to regexp-match an IP</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1940164104" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>[[ "123.2.3.4" =~ ^[[:digit:]]&lbrace;1,3&rbrace;\.[[:digit:]]&lbrace;1,3&rbrace;\.[[:digit:]]&lbrace;1,3&rbrace;\.[[:digit:]]&lbrace;1,3&rbrace;$ ]] &amp;&amp; echo "Its an ip"</p></div>
</div>
<div ID="freeplaneNode__ID_1858628709" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Bash &quot;ternary&quot; </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1201390152" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>mycommand &amp;&amp; echo "true" || echo "false"</p></div>
</div>
<div ID="freeplaneNode__ID_999488166" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Bash arrays </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_913761609" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>arr=(Hello World)</p>
<p>arr[0]=Hello</p>
<p>arr[1]=World</p>
<p></p>
<p>$&lbrace;arr[*]&rbrace;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# All of the items in the array</p>
<p>$&lbrace;!arr[*]&rbrace;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# All of the indexes in the array</p>
<p>$&lbrace;#arr[*]&rbrace;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Number of items in the array</p>
<p>$&lbrace;#arr[0]&rbrace;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Length of item zero</p>
<p></p>
<p>echo $&lbrace;arr[0]&rbrace; $&lbrace;arr[1]&rbrace;</p></div>
</div>
<div ID="freeplaneNode__ID_970717281" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>#!/bin/bash</p>
<p></p>
<p>array=(one two three four [5]=five)</p>
<p></p>
<p>echo "Array size: $&lbrace;#array[*]&rbrace;"</p>
<p></p>
<p>echo "Array items:"</p>
<p>for item in $&lbrace;array[*]&rbrace;</p>
<p>do</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;printf "&nbsp;&nbsp;&nbsp;%s\n" $item</p>
<p>done</p>
<p></p>
<p>echo "Array indexes:"</p>
<p>for index in $&lbrace;!array[*]&rbrace;</p>
<p>do</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;printf "&nbsp;&nbsp;&nbsp;%d\n" $index</p>
<p>done</p>
<p></p>
<p>echo "Array items and indexes:"</p>
<p>for index in $&lbrace;!array[*]&rbrace;</p>
<p>do</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;printf "%4d: %s\n" $index $&lbrace;array[$index]&rbrace;</p>
<p>done</p></div>
</div>
<div ID="freeplaneNode__ID_1817666314" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p># array with lines of file</p>
<p># https://stackoverflow.com/questions/30988586/creating-an-array-from-a-text-file-in-bash</p>
<p>mapfile -t myArray &lt; EachLineOfThisFileWillGoIntoAnArrayElement.txt</p></div>
</div>
<div ID="freeplaneNode__ID_1020108676" class=FreeplaneNodeGen>
    <p>
      <a href="http://www.linuxjournal.com/content/bash-arrays">http://www.linuxjournal.com/content/bash-arrays</a><br>
    </p>
</div>
<div ID="freeplaneNode__ID_1447798071" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Bash background processes &amp;</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1686223885" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      #!/bin/bash
    </p>
    <p>
      set -o errexit
    </p>
    <p>
<br>
    </p>
    <p>
      java MyProg 1 <font color="#ffffff">&amp; </font>
    </p>
    <p>
      <font color="#ffffff">pid1=$!</font>
    </p>
    <p>
      java MyProg 0 <font color="#ffffff">&amp; </font>
    </p>
    <p>
      <font color="#ffffff">pid2=$!</font>
    </p>
    <p>
      java MyProg 2 <font color="#ffffff">&amp; </font>
    </p>
    <p>
      <font color="#ffffff">pid3=$!</font>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#ffffff">wait $pid1</font>&#160;&amp;&amp; echo &quot;pid1 exited normally&quot; || echo &quot;pid1 exited abnormally with status $?&quot;
    </p>
    <p>
      <font color="#ffffff">wait $pid2</font>&#160;&amp;&amp; echo &quot;pid2 exited normally&quot; || echo &quot;pid2 exited abnormally with status $?&quot;
    </p>
    <p>
      <font color="#ffffff">wait $pid3</font>&#160;&amp;&amp; echo &quot;pid3 exited normally&quot; || echo &quot;pid3 exited abnormally with status $?&quot;
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_83373920" class=FreeplaneNodeGen>
    <p>
      <a href="http://stackoverflow.com/questions/6156541/threads-in-bash">http://stackoverflow.com/questions/6156541/threads-in-bash</a>
    </p>
</div>
<div ID="freeplaneNode__ID_1509423126" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Make xargs execute the command once for each argument</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_626801854" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      $ echo one two &quot;three and four&quot; | <font color="#ffffff">xargs -n 1</font>&#160;<font color="#cc99ff">./show</font>
    </p>
    <p>
      -&gt; &quot;one&quot;
    </p>
    <p>
      -&gt; &quot;two&quot;
    </p>
    <p>
      -&gt; &quot;three&quot;
    </p>
    <p>
      -&gt; &quot;and&quot;
    </p>
    <p>
      -&gt; &quot;four&quot;
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1944446290" class=FreeplaneNodeGen>
    <p>
      Where <font color="#cc99ff">./show</font>&#160;contains:
    </p>
</div>
<div ID="freeplaneNode__ID_1115599858" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>#!/bin/bash</p>
<p>echo -n "-&gt; "; for a in "$@"; do echo -n "\"$a\" "; done; echo</p></div>
</div>
<div ID="freeplaneNode__ID_211551650" class=FreeplaneNodeGen>
<p></p></div>
<div ID="freeplaneNode__ID_1946720860" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Readline cheat-sheet</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_527174706" class=FreeplaneNodeGen>
<p>png_7822920995783219251.png</p>
{{< figure
    src="png_7822920995783219251.png"
    link="png_7822920995783219251.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_228705766" class=FreeplaneNodeGen>
    <p>
      <a href="http://readline.kablamo.org/emacs.html">http://readline.kablamo.org/emacs.html</a>
    </p>
</div>
<div ID="freeplaneNode__ID_1742837504" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Bash redirection cheat-sheet (advanced) :)</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1498278334" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_501828285034217583.png"
    link="png_501828285034217583.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1285578075" class=FreeplaneNodeGen>
    <p>
      <a href="https://catonmat.net/ftp/bash-redirections-cheat-sheet.pdf">https://catonmat.net/ftp/bash-redirections-cheat-sheet.pdf</a>
    </p>
</div>
</div>
