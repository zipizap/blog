---
title: "Ansible tip to workaround single-pass-variable-expansion"
date: 2019-09-06 


draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
#- Cloud
#- Programming

tags:
- 4hackers
- 4nonhackers
- informative
#- exploitation
#- suid
#- etc

---
<div>
<div ID="freeplaneNode__ID_382367239" class=FreeplaneNodeGen>
    <p>
      Or how to achieve the equivalent effect of:
    </p>
    <p>
      &#160;&#160;<font color="#0000c0">&lbrace;&lbrace;</font>&#160;<font color="#ff0000">&lbrace;&lbrace;</font>myVar1<font color="#ff0000">&rbrace;&rbrace;</font>.myField <font color="#0000c0">&rbrace;&rbrace;</font>
    </p>
    <p>
      <br>
      <br>
      The variable-expansion in ansible playbooks is limited to only 1 pass. (see [1])
    </p>
    <p>
      So its not possible to expand <font color="#0000c0">&lbrace;&lbrace;</font>&#160;<font color="#ff0000">&lbrace;&lbrace;</font>myVAR1<font color="#ff0000">&rbrace;&rbrace;</font>.something <font color="#0000c0">&rbrace;&rbrace;</font>&#160;as this would need 2 expansions &lbrace;&lbrace; &lbrace;&lbrace;1&rbrace;&rbrace; &rbrace;&rbrace; and this does not happen
    </p>
    <p>
<br>
    </p>
    <p>
      The workaround is to try to use &quot;&lbrace;&lbrace; var<font color="#0000c0">[]</font><font color="#ff0000">[]</font>&#160;&rbrace;&rbrace;&quot; so that in one single pass it all gets expanded and executes correctly. They are now multiple expansions executed linearly, not nested, and so the single-pass-expasion will resolve them all, as we want
    </p>
    <p>
      <br>
      <br>
<br>
    </p>
    <p>
      &lbrace;&lbrace; vars[toDCe]['name']&rbrace;&rbrace;
    </p>
    <p>
      -- expands to --
    </p>
    <p>
      &lbrace;&lbrace; vars[&quot;DCe&quot;]['name']&rbrace;&rbrace;
    </p>
    <p>
      -- which evaluates to --
    </p>
    <p>
      &quot;###---DCeNAME---###&quot;
    </p>
    <p>
      which is what we want <b>:) </b>
    </p>
    <p>
<br>
    </p>
    <p>
      I've left it resumed in this example
    </p>
</div>
<div ID="freeplaneNode__ID_1361028745" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_1376239202598864061.png"
    link="png_1376239202598864061.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1088020985" class=FreeplaneNodeGen>
<p>[1]: https://stackoverflow.com/questions/29276198/ansible-how-to-construct-a-variable-from-another-variable-and-then-fetch-its-v</p></div>
</div>
