---
title: "Kubernetes quick-lab with K3d"
date: 2021-08-11


draft: false

categories:
#- Infosec
- Devops
- Sysadmin
- Cloud
- K8s
#- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
#- cve
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
#- bash
#- ansible
#- quickref
- k8s
- k3s
- k3d
---
<div>
<div ID="freeplaneNode__ID_190347917" class=FreeplaneNodeGen>
    <p>
      Some time ago, I needed to have some way to <u>quickly spin up a k8s 
      cluster</u>, make some tests, and delete it.
    </p>
    <p>
<br>
    </p>
    <p>
      I also wanted it to be <u>lighweight</u>&nbsp;in ram - so to run it in a small 
      VPS that could not support nested-virtualization. And be as close as 
      posible to a &quot;real&quot; k8s cluster (multi-node, updated for newer versions, 
      etc)
    </p>
    <p>
      <br>
      <br>
      <b><font size="4">K3d</font></b><br><br>
    </p>
    <p>
      After searching for options, I decided to use <a href="https://k3d.io/">k3d</a>&nbsp;:<br>&nbsp;&nbsp;- is 
      built from k3s, maintained/updated by rancher-labs, and k8s compliant<br>&nbsp;&nbsp;- 
      runs over docker: k3d is <b>k3</b>s-over-<b>d</b>ocker<br>&nbsp;&nbsp;- can support 
      multi-nodes (as separate docker containers)<br>&nbsp;&nbsp;- only requires Docker, 
      is light-weight (~2gb ram for 1-node cluster running iddle)
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      For this demo, I created a temporary VM in Azure cloud (D2s v4) with 8GB 
      ram, running ubuntu20.04.<br>Vms in Azure cost quite a bit, but with 
      terraform they can be created instantaneously, and easily deleted in the 
      end - so for some minutes/hours its very usefull. After finishing the 
      demo I'll delete the VM to avoid its costs<br>
    </p>
    <p>
      A much cheaper VM to keep running long-term can be found in contaboo, 
      muuuuch cheaper (1year of contaboo = 1 month of azure) and works well 
      long-term :)
    </p>
    <p>
      <br>
      <br>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b><font size="4">zipizap/k3d-lab</font></b><br><br>To quickly setup the cluster (and make life 
      easier for my future-self), I've prepared some scripts in this git repo: <a href="https://github.com/zipizap/k3d-lab">https://github.com/zipizap/k3d-lab&nbsp; 
      </a>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      We will use these to setup k3d and a minimal k8s-cluster with 1 
      master-node which will also be running workloads. Different setups are 
      possible, with multiple master-nodes and worker-nodes, but this was the 
      minimal-memory setup :)
    </p>
    <p>
<br>
    </p>
    <p>
      Also, the scripts configure an additional external docker-container <b>host-local-mirror-registry 
      </b>that acts as a docker-hub-caching-mirror, to avoid re-downloading 
      images from docker-hub. This accelerates pod re-creation
    </p>
    <p>
<br>
    </p>
    <p>
      The scripts are bash, and I've added additional debugging options to see 
      what command is being executed at each moment - the '███████...' lines. 
      Just ignore them, or have a look :)
    </p>
</div>
<div ID="freeplaneNode__ID_1489677863" class=FreeplaneNodeGen>
<p>Lets measure the memory usage to compare when in the end we have a running k3d kubernetes cluster</p></div>
<div ID="freeplaneNode__ID_1330202475" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~$ <font color="#ffffff">free -h</font>
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;used&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;free&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shared&nbsp;&nbsp;buff/cache&nbsp;&nbsp;&nbsp;available
    </p>
    <p>
      Mem:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.8Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#ffffff">200Mi</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.3Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.0Mi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;287Mi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.3Gi
    </p>
    <p>
      Swap:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_362276690" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      Requisites: install <b>docker </b><br>
    </p>
</div>
<div ID="freeplaneNode__ID_966225439" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#ffffff">sudo apt-get update </font>
    </p>
    <p>
      <font color="#ffffff">sudo apt-get -y install \</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;apt-transport-https \</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;ca-certificates \</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;curl \</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;gnupg \</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;lsb-release </font>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#ffffff">curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg 
      --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg </font>
    </p>
    <p>
      <font color="#ffffff">echo \</font>
    </p>
    <p>
      <font color="#ffffff">&quot;deb [arch=amd64 
      signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] 
      https://download.docker.com/linux/ubuntu \</font>
    </p>
    <p>
      <font color="#ffffff">$(lsb_release -cs) stable&quot; | sudo tee 
      /etc/apt/sources.list.d/docker.list &gt; /dev/null </font>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#ffffff">sudo apt-get update</font>
    </p>
    <p>
      <font color="#ffffff">sudo apt-get -y install docker-ce docker-ce-cli containerd.io</font>
    </p>
    <p>
      <font color="#ffffff">sudo usermod -aG docker $USER</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_384833189" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#ffffff">newgrp docker</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1947691204" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      Install kubectl, helm k9s&nbsp; <font size="1">the quickest-way :)</font>
    </p>
</div>
<div ID="freeplaneNode__ID_1496122220" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#ffffff">sudo snap install kubectl --classic </font>
    </p>
    <p>
      <font color="#ffffff">sudo snap install helm --classic </font>
    </p>
    <p>
      <font color="#ffffff">curl -Ls https://api.github.com/repos/derailed/k9s/releases/latest | 
      grep -wo &quot;https.*k9s_Linux_x86_64.tar.gz&quot; | xargs curl -sL | tar xvz k9s</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_114051104" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      Git clone <b>k3d-lab</b>
    </p>
</div>
<div ID="freeplaneNode__ID_1626162211" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~$&nbsp;<font color="#ffffff">git clone https://github.com/zipizap/k3d-lab.git</font>
    </p>
    <p>
      Cloning into 'k3d-lab'...
    </p>
    <p>
      remote: Enumerating objects: 464, done.
    </p>
    <p>
      remote: Counting objects: 100% (464/464), done.
    </p>
    <p>
      remote: Compressing objects: 100% (331/331), done.
    </p>
    <p>
      remote: Total 464 (delta 138), reused 432 (delta 109), pack-reused 0
    </p>
    <p>
      Receiving objects: 100% (464/464), 269.25 KiB | 13.46 MiB/s, done.
    </p>
    <p>
      Resolving deltas: 100% (138/138), done.
    </p>
    <p>
      uzer@vm-client:~$&nbsp;<font color="#ffffff">cd k3d-lab/</font>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">ls -l</font>
    </p>
    <p>
      total 116
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;114 Aug 11 08:23 <font color="#00b6c6">0.install_k3d-4.4.7.sh</font>
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;2527 Aug 11 08:23 <font color="#00b6c6">1.cluster_create.sh</font>
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;&nbsp;32 Aug 11 08:23 1.cluster_delete.sh
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;&nbsp;44 Aug 11 08:23 1.cluster_recreate.sh
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;296 Aug 11 08:23 <font color="#00b6c6">2.cluster_lb_externalip.sh</font>
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;&nbsp;52 Aug 11 08:23 <font color="#00b6c6">2.cluster_start.sh</font>
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;&nbsp;97 Aug 11 08:23 <font color="#00b6c6">2.cluster_stats.sh</font>
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;&nbsp;30 Aug 11 08:23 2.cluster_stop.sh
    </p>
    <p>
      -rw-rw-r-- 1 uzer uzer 35149 Aug 11 08:23 LICENSE
    </p>
    <p>
      -rw-rw-r-- 1 uzer uzer&nbsp;&nbsp;&nbsp;&nbsp;18 Aug 11 08:23 README.md
    </p>
    <p>
      drwxrwxr-x 2 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 dashboard
    </p>
    <p>
      drwxrwxr-x 3 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 helmDeployHashLockedSecret
    </p>
    <p>
      drwxrwxr-x 2 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 host-local-mirror-registry
    </p>
    <p>
      drwxrwxr-x 2 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 hpa
    </p>
    <p>
      drwxrwxr-x 4 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 istios
    </p>
    <p>
      -rw-rw-r-- 1 uzer uzer&nbsp;&nbsp;&nbsp;625 Aug 11 08:23 k3d.source
    </p>
    <p>
      -rwxrwxr-x 1 uzer uzer&nbsp;&nbsp;&nbsp;678 Aug 11 08:23 k9s.sh
    </p>
    <p>
      drwxrwxr-x 4 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 manifests
    </p>
    <p>
      -rw-rw-r-- 1 uzer uzer&nbsp;&nbsp;&nbsp;212 Aug 11 08:23 registries.yaml
    </p>
    <p>
      drwxrwxr-x 2 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 resources-demo
    </p>
    <p>
      drwxrwxr-x 3 uzer uzer&nbsp;&nbsp;4096 Aug 11 08:23 sa-podlogs
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_377545341" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      <b>Notes: </b>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;- In <b>0.install_k3d-4.4.7.sh</b>&nbsp;
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;-- 4.4.7 is the current version of k3d,&nbsp;however for the future that 
      is easy to change in script as there is a TAG
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;- In <a href="https://github.com/zipizap/k3d-lab/blob/main/1.cluster_create.sh">1.cluster_create.sh</a>&nbsp;
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;-- the <b>SERVERS_NUM </b>and <b>AGENTS_NUM </b>define 
      the number of nodes for the k3d-cluster - change them as you prefer
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;-- if you want to <u>disable the additional 
      external docker-container host-local-mirror-registry</u>&nbsp;that acts as a 
      docker-hub-caching-mirror, remove the 2 lines indicated in image bellow
    </p>
</div>
<div ID="freeplaneNode__ID_21739545" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_2971451493000546425.png"
    link="png_2971451493000546425.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_492779381" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;<b>&nbsp;</b>
    </p>
</div>
<div ID="freeplaneNode__ID_117199597" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$&nbsp;<font color="#ffffff">./0.install_k3d-4.4.7.sh
</font>    </p>
    <p>
      + wget -q -O - 
      https://raw.githubusercontent.com/rancher/k3d/main/install.sh
    </p>
    <p>
      + TAG=v4.4.7
    </p>
    <p>
      + bash
    </p>
    <p>
      Preparing to install k3d into /usr/local/bin
    </p>
    <p>
      k3d installed into /usr/local/bin/k3d
    </p>
    <p>
      Run 'k3d --help' to see what you can do with it.
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1435357194" class=FreeplaneNodeGen>
<p>then</p></div>
<div ID="freeplaneNode__ID_150261601" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$&nbsp;<font color="#ffffff">./1.cluster_create.sh </font>
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@[18]&gt;&nbsp;&nbsp;SERVERS_NUM=1
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@[21]&gt;&nbsp;&nbsp;AGENTS_NUM=0
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@[32]&gt;&nbsp;&nbsp;K3D_ADDITIONAL_OPTS=
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@[82]&gt;&nbsp;&nbsp;main
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@main[49]&gt;&nbsp;&nbsp;cd 
      /home/uzer/k3d-lab
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@main[51]&gt;&nbsp; 
      CLUSTER_NAME=myk3dcluster
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@main[52]&gt;&nbsp;&nbsp;k3d cluster 
      create myk3dcluster --servers=1 --agents=0 --volume 
      /home/uzer/k3d-lab/registries.yaml:/etc/rancher/k3s/registries.yaml 
      --wait
    </p>
    <p>
      WARN[0000] No node filter specified
    </p>
    <p>
      INFO[0000] Prep: Network
    </p>
    <p>
      INFO[0000] Created network 'k3d-myk3dcluster' 
      (8637a026e796ba3aa03ad393de5d5c67d43df81b73eec5ad73a34c34866626be)
    </p>
    <p>
      INFO[0000] Created volume 'k3d-myk3dcluster-images'
    </p>
    <p>
      ...
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@main[73]&gt;&nbsp;&nbsp;echo '== 
      breathing a couple of mins before checking final state ...'
    </p>
    <p>
      == breathing a couple of mins before checking final state ...
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@main[74]&gt;&nbsp;&nbsp;sleep 60
    </p>
    <p>
      ████████████████████████./1.cluster_create.sh@main[75]&gt;&nbsp;&nbsp;<font color="#00ffcc">kubectl 
      get pod,service -A</font>
    </p>
    <p>
      <font color="#00ffcc">NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp; 
      STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RESTARTS&nbsp;&nbsp;&nbsp;AGE </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/metrics-server-86cbb8457f-hhgfq&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; </font><font color="#cc99ff">Running</font><font color="#00ffcc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;58s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;&lt; 
      at the end of the script the pods should be booting or running already </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/local-path-provisioner-5ff76fc89d-q9q8h&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;58s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/coredns-7448499f4d-qkh4g&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;58s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/helm-install-traefik-crd-qm97l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Completed&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;59s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/helm-install-traefik-79kcc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Completed&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;59s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/svclb-traefik-r6vx6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2/2&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;34s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;pod/traefik-97b44b794-cdvb8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;34s </font>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#00ffcc">NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER-IP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      EXTERNAL-IP&nbsp;&nbsp;&nbsp;PORT(S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AGE </font>
    </p>
    <p>
      <font color="#00ffcc">default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;service/kubernetes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;73s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;service/kube-dns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;53/UDP,53/TCP,9153/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;70s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;service/metrics-server&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.255.69&nbsp;&nbsp;&nbsp; 
      &lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;70s </font>
    </p>
    <p>
      <font color="#00ffcc">kube-system&nbsp;&nbsp;&nbsp;service/traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LoadBalancer&nbsp;&nbsp;&nbsp;10.43.113.182&nbsp;&nbsp; </font><font color="#cc99ff">172.18.0.2</font><font color="#00ffcc">&nbsp;&nbsp;&nbsp;&nbsp;80:30219/TCP,443:30420/TCP&nbsp;&nbsp;&nbsp;34s 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;&lt; the external-ip for the loadbalancer (via bridge in host)</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_276723443" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      This last script will create docker a k3d-cluster named &quot;myk3dcluster&quot; 
      in docker containers, and write-out the kubeconfig into <b>./kubeconfig.yaml, 
      </b>which can be easily loaded with the <b>k3d.source</b>&nbsp;script.<br><br>
    </p>
    <p>
      <a href="https://github.com/zipizap/k3d-lab/blob/main/k3d.source">k3d.source</a>&nbsp;is a handy script to configure the current bash-session 
      to:
    </p>
    <p>
      &nbsp;&nbsp;- export KUBECONFIG env-var that points to the k3d-kubernetes-cluster 
      just created
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;This is to avoid messing up any other existing kubeconfig file
    </p>
    <p>
      &nbsp;&nbsp;- setup bash-completion for <u>kubectl</u>&nbsp;and <u>k</u>&nbsp;(alias)
    </p>
    <p>
      &nbsp;&nbsp;- setup bash-completion for <u>helm</u>&nbsp;and <u>h</u>&nbsp;(alias)
    </p>
    <p>
      &nbsp;&nbsp;- if istio binary exists, add it into the path
    </p>
    <p>
      &nbsp;&nbsp;- setup bash-completion for k3d
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;The bash-completion for k and h are veeery usefull ;)
    </p>
    <p>
      &nbsp;&nbsp;<font size="1">tip: shell-geeks should also check-out <a href="https://github.com/dty1er/kubecolor">kubecolor</a>&nbsp;;) </font>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;Just &quot;<b>source k3d.source&quot; </b>and from then on use <b>kubectl 
      </b>or <b>helm </b>normally
    </p>
</div>
<div ID="freeplaneNode__ID_6140861" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$&nbsp;<font color="#ffffff">source k3d.source</font>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$&nbsp;<font color="#ffffff">kubectl get all -A</font>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp; 
      STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RESTARTS&nbsp;&nbsp;&nbsp;AGE 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/metrics-server-86cbb8457f-hhgfq&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m46s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/local-path-provisioner-5ff76fc89d-q9q8h&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m46s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/coredns-7448499f4d-qkh4g&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m46s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/helm-install-traefik-crd-qm97l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Completed&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m47s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/helm-install-traefik-79kcc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Completed&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m47s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/svclb-traefik-r6vx6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2/2&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m22s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/traefik-97b44b794-cdvb8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp; 
      Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m22s 
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER-IP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      EXTERNAL-IP&nbsp;&nbsp;&nbsp;PORT(S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AGE 
    </p>
    <p>
      default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;service/kubernetes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7m1s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;service/kube-dns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;53/UDP,53/TCP,9153/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m58s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;service/metrics-server&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.255.69&nbsp;&nbsp;&nbsp; 
      &lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m58s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;service/traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LoadBalancer&nbsp;&nbsp;&nbsp;10.43.113.182&nbsp;&nbsp; 
      172.18.0.2&nbsp;&nbsp;&nbsp;&nbsp;80:30219/TCP,443:30420/TCP&nbsp;&nbsp;&nbsp;6m22s 
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DESIRED&nbsp;&nbsp;&nbsp;CURRENT&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp; 
      UP-TO-DATE&nbsp;&nbsp;&nbsp;AVAILABLE&nbsp;&nbsp;&nbsp;NODE SELECTOR&nbsp;&nbsp;&nbsp;AGE 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;daemonset.apps/svclb-traefik&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m22s 
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp; 
      UP-TO-DATE&nbsp;&nbsp;&nbsp;AVAILABLE&nbsp;&nbsp;&nbsp;AGE 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/metrics-server&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m58s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/local-path-provisioner&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m58s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/coredns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m58s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m22s 
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      DESIRED&nbsp;&nbsp;&nbsp;CURRENT&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;AGE 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/metrics-server-86cbb8457f&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m47s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/local-path-provisioner-5ff76fc89d&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m47s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/coredns-7448499f4d&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m47s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/traefik-97b44b794&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6m22s 
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPLETIONS&nbsp;&nbsp; 
      DURATION&nbsp;&nbsp;&nbsp;AGE 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;job.batch/helm-install-traefik-crd&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;22s&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;6m58s 
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;job.batch/helm-install-traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25s&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;6m58s
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_488034957" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">helm list -A</font>
    </p>
    <p>
      <font color="#00ff00">NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REVISION&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UPDATED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHART&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APP 
      VERSION
</font>    </p>
    <p>
      <font color="#00ff00">traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kube-system&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2021-08-11 
      11:55:24.016983296 +0000 UTC 
      deployed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;traefik-9.18.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.4.8
</font>    </p>
    <p>
      <font color="#00ff00">traefik-crd&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kube-system&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2021-08-11 
      11:55:21.069623904 +0000 UTC deployed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;traefik-crd-9.18.2</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1639895172" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      Lets launch a nginx pod<b>:</b>
    </p>
</div>
<div ID="freeplaneNode__ID_1451947070" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl run nginx --image=nginx</font>
    </p>
    <p>
      pod/nginx created
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl get pod</font>
    </p>
    <p>
      <font color="#999999">NAME&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RESTARTS&nbsp;&nbsp;&nbsp;AGE </font>
    </p>
    <p>
      <font color="#999999">nginx&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#00b6c6">ContainerCreating</font><font color="#999999">&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8s</font>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl get pod</font>
    </p>
    <p>
      <font color="#999999">NAME&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;STATUS&nbsp;&nbsp;&nbsp;&nbsp;RESTARTS&nbsp;&nbsp;&nbsp;AGE </font>
    </p>
    <p>
      <font color="#999999">nginx&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#00b6c6">Running</font><font color="#999999">&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10s</font>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl get pod</font>
    </p>
    <p>
      <font color="#999999">NAME&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;STATUS&nbsp;&nbsp;&nbsp;&nbsp;RESTARTS&nbsp;&nbsp;&nbsp;AGE </font>
    </p>
    <p>
      <font color="#999999">nginx&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14s</font>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl describe pod nginx</font>
    </p>
    <p>
      <font color="#999999">Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nginx </font>
    </p>
    <p>
      <font color="#999999">Namespace:&nbsp;&nbsp;&nbsp;&nbsp;default </font>
    </p>
    <p>
      <font color="#999999">Priority:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0 </font>
    </p>
    <p>
      <font color="#999999">Node:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;k3d-myk3dcluster-server-0/172.18.0.2 </font>
    </p>
    <p>
      <font color="#999999">Start Time:&nbsp;&nbsp;&nbsp;Wed, 11 Aug 2021 12:09:06 +0000 </font>
    </p>
    <p>
      <font color="#999999">Labels:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;run=nginx </font>
    </p>
    <p>
      <font color="#999999">Annotations:&nbsp;&nbsp;&lt;none&gt; </font>
    </p>
    <p>
      <font color="#999999">Status:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running </font>
    </p>
    <p>
      <font color="#999999">IP:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.42.0.9 </font>
    </p>
    <p>
      <font color="#999999">IPs: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;IP:&nbsp;&nbsp;10.42.0.9 </font>
    </p>
    <p>
      <font color="#999999">Containers: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;nginx: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Container 
      ID:&nbsp;&nbsp;&nbsp;containerd://0a001155c4f26be35349198a85fe89f2704f2e525a67a48890f73bdda886a37e 
      </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Image:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nginx </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Image 
      ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docker.io/library/nginx@sha256:8f335768880da6baf72b70c701002b45f4932acae8d574dedfddaf967fc3ac90 
      </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Port:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Host Port:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;State:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Started:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wed, 11 Aug 2021 12:09:14 +0000 </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Ready:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Restart Count:&nbsp;&nbsp;0 </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Environment:&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Mounts: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/var/run/secrets/kubernetes.io/serviceaccount from 
      kube-api-access-5p7mc (ro) </font>
    </p>
    <p>
      <font color="#999999">Conditions: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Initialized&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;ContainersReady&nbsp;&nbsp;&nbsp;True </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;PodScheduled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True </font>
    </p>
    <p>
      <font color="#999999">Volumes: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;kube-api-access-5p7mc: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Projected (a volume that contains 
      injected data from multiple sources) </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;TokenExpirationSeconds:&nbsp;&nbsp;3607 </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;ConfigMapName:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kube-root-ca.crt </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;ConfigMapOptional:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;nil&gt; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;DownwardAPI:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;true </font>
    </p>
    <p>
      <font color="#999999">QoS Class:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BestEffort </font>
    </p>
    <p>
      <font color="#999999">Node-Selectors:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt; </font>
    </p>
    <p>
      <font color="#999999">Tolerations:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;node.kubernetes.io/not-ready:NoExecute 
      op=Exists for 300s </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;node.kubernetes.io/unreachable:NoExecute 
      op=Exists for 300s </font>
    </p>
    <p>
      <font color="#999999">Events: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;Reason&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age&nbsp;&nbsp;&nbsp;From&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Message </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;----&nbsp;&nbsp;&nbsp;&nbsp;------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----&nbsp;&nbsp;----&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;------- </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Scheduled&nbsp;&nbsp;47s&nbsp;&nbsp;&nbsp;default-scheduler&nbsp;&nbsp;Successfully assigned 
      default/nginx to k3d-myk3dcluster-server-0 </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Pulling&nbsp;&nbsp;&nbsp;&nbsp;48s&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pulling image &quot;nginx&quot; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Pulled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;43s&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Successfully </font><font color="#00b6c6">pulled 
      image &quot;nginx&quot; in 4.398613616s </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Created&nbsp;&nbsp;&nbsp;&nbsp;40s&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Created container nginx </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Started&nbsp;&nbsp;&nbsp;&nbsp;40s&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Started container nginx</font>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1488946292" class=FreeplaneNodeGen>
<p>Lets check that a second-pod with same image downloads the image much faster</p>
<p>NOTE:</p>
<p>&nbsp;&nbsp;- within-a-cluster the images are normally cached by CRI of k8s-nodes, so the host-local-mirror-registry does not help much</p>
<p>&nbsp;&nbsp;- with different k3d-clusters (create new, or multiple independent) that use the host-local-mirror-registry, then there will be a speedup as the k8s-nodes will need to download a few images and will be able to get them from the host-local-mirror-registry cache&nbsp;&nbsp;instead of going all the way to docker-hub</p></div>
<div ID="freeplaneNode__ID_312830121" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl run nginx2 --image=nginx</font>
    </p>
    <p>
      pod/nginx2 created
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl describe pod nginx2</font>
    </p>
    <p>
      <font color="#999999">Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nginx2
</font>    </p>
    <p>
      <font color="#999999">Namespace:&nbsp;&nbsp;&nbsp;&nbsp;default
</font>    </p>
    <p>
      <font color="#999999">Priority:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0
</font>    </p>
    <p>
      <font color="#999999">Node:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;k3d-myk3dcluster-server-0/172.18.0.2
</font>    </p>
    <p>
      <font color="#999999">Start Time:&nbsp;&nbsp;&nbsp;Wed, 11 Aug 2021 12:12:34 +0000
</font>    </p>
    <p>
      <font color="#999999">Labels:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;run=nginx2
</font>    </p>
    <p>
      <font color="#999999">Annotations:&nbsp;&nbsp;&lt;none&gt;
</font>    </p>
    <p>
      <font color="#999999">Status:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running
</font>    </p>
    <p>
      <font color="#999999">IP:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.42.0.10
</font>    </p>
    <p>
      <font color="#999999">IPs:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;IP:&nbsp;&nbsp;10.42.0.10
</font>    </p>
    <p>
      <font color="#999999">Containers:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;nginx2:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Container 
      ID:&nbsp;&nbsp;&nbsp;containerd://a71a452d47e1ad61d4a2dd76f451babcf182916df882a702cfa1012ae336913f
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Image:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;nginx
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Image 
      ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;docker.io/library/nginx@sha256:8f335768880da6baf72b70c701002b45f4932acae8d574dedfddaf967fc3ac90
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Port:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Host Port:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;State:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Started:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wed, 11 Aug 2021 12:12:35 +0000
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Ready:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Restart Count:&nbsp;&nbsp;0
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Environment:&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Mounts:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/var/run/secrets/kubernetes.io/serviceaccount from 
      kube-api-access-t5z77 (ro)
</font>    </p>
    <p>
      <font color="#999999">Conditions:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Initialized&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;ContainersReady&nbsp;&nbsp;&nbsp;True
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;PodScheduled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;True
</font>    </p>
    <p>
      <font color="#999999">Volumes:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;kube-api-access-t5z77:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;Type:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Projected (a volume that contains 
      injected data from multiple sources)
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;TokenExpirationSeconds:&nbsp;&nbsp;3607
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;ConfigMapName:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kube-root-ca.crt
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;ConfigMapOptional:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;nil&gt;
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;DownwardAPI:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;true
</font>    </p>
    <p>
      <font color="#999999">QoS Class:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BestEffort
</font>    </p>
    <p>
      <font color="#999999">Node-Selectors:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;
</font>    </p>
    <p>
      <font color="#999999">Tolerations:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;node.kubernetes.io/not-ready:NoExecute 
      op=Exists for 300s
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;node.kubernetes.io/unreachable:NoExecute 
      op=Exists for 300s
</font>    </p>
    <p>
      <font color="#999999">Events:
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;Reason&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Age&nbsp;&nbsp;&nbsp;From&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Message
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;----&nbsp;&nbsp;&nbsp;&nbsp;------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----&nbsp;&nbsp;----&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-------
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Scheduled&nbsp;&nbsp;4s&nbsp;&nbsp;&nbsp;&nbsp;default-scheduler&nbsp;&nbsp;Successfully assigned 
      default/nginx2 to k3d-myk3dcluster-server-0
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Pulling&nbsp;&nbsp;&nbsp;&nbsp;3s&nbsp;&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pulling image &quot;nginx&quot;
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Pulled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3s&nbsp;&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Successfully </font><font color="#00b6c6">pulled 
      image &quot;nginx&quot; in 194.704163ms</font><font color="#999999">
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Created&nbsp;&nbsp;&nbsp;&nbsp;3s&nbsp;&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Created container nginx2
</font>    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;Normal&nbsp;&nbsp;Started&nbsp;&nbsp;&nbsp;&nbsp;3s&nbsp;&nbsp;&nbsp;&nbsp;kubelet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Started container nginx2</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1290151490" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      The k8s cluster is now using around <b>1,2GB </b>(all included, which is 
      not bad for an operational k8s cluster)
    </p>
</div>
<div ID="freeplaneNode__ID_936649420" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">free -h</font>
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;used&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;free&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shared&nbsp;&nbsp;buff/cache&nbsp;&nbsp;&nbsp;available
    </p>
    <p>
      Mem:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.8Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#00b6c6">1.2Gi&nbsp;</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.3Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.0Mi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.3Gi
    </p>
    <p>
      Swap:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B
    </p>
    <p>
<br>
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">./2.cluster_stats.sh</font>
    </p>
    <p>
      NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SERVERS&nbsp;&nbsp;&nbsp;AGENTS&nbsp;&nbsp;&nbsp;LOADBALANCER
    </p>
    <p>
      myk3dcluster&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;true
    </p>
    <p>
      NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ROLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STATUS
    </p>
    <p>
      k3d-myk3dcluster-server-0&nbsp;&nbsp;&nbsp;server&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;myk3dcluster&nbsp;&nbsp;&nbsp;running
    </p>
    <p>
      k3d-myk3dcluster-serverlb&nbsp;&nbsp;&nbsp;loadbalancer&nbsp;&nbsp;&nbsp;myk3dcluster&nbsp;&nbsp;&nbsp;running
    </p>
    <p>
      CONTAINER ID&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CPU %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEM USAGE / LIMIT&nbsp; 
      &nbsp;&nbsp;&nbsp;MEM %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NET I/O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BLOCK I/O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PIDS
    </p>
    <p>
      dd5faf320669&nbsp;&nbsp;&nbsp;host-local-mirror-registry&nbsp;&nbsp;&nbsp;0.06%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;52.23MiB / 
      7.775GiB&nbsp;&nbsp;&nbsp;0.66%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;347MB / 174MB&nbsp;&nbsp;&nbsp;0B / 173MB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12
    </p>
    <p>
      6f2c332e39fb&nbsp;&nbsp; <font color="#00b6c6">k3d-myk3dcluster-serverlb&nbsp;&nbsp;&nbsp;&nbsp;0.00%&nbsp;&nbsp;&nbsp;&nbsp; 
      3.617MiB / 7.775GiB&nbsp;&nbsp;&nbsp;0.05%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;814kB / 809kB&nbsp;&nbsp;&nbsp;0B / 0B</font>&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5
    </p>
    <p>
      9db0a4af2982&nbsp;&nbsp; <font color="#00b6c6">k3d-myk3dcluster-server-0&nbsp;&nbsp;&nbsp;&nbsp;7.62%&nbsp;&nbsp;&nbsp;&nbsp; 
      947.3MiB / 7.775GiB&nbsp;&nbsp;&nbsp;11.90%&nbsp;&nbsp;&nbsp;&nbsp;173MB / 801kB&nbsp;&nbsp;&nbsp;8.19kB / 221MB</font>&nbsp;
      &nbsp;&nbsp;162
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER-IP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      EXTERNAL-IP&nbsp;&nbsp;&nbsp;PORT(S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kubernetes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;kube-dns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;53/UDP,53/TCP,9153/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;metrics-server&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.255.69&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp; 
      &nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LoadBalancer&nbsp;&nbsp;&nbsp;10.43.113.182&nbsp;&nbsp;&nbsp;172.18.0.2 
      &nbsp;&nbsp;&nbsp;80:30219/TCP,443:30420/TCP&nbsp;&nbsp;&nbsp;20m
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_939721366" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      We can extract the LoadBalander external-ip<b>:</b>
    </p>
</div>
<div ID="freeplaneNode__ID_765019520" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ LB_IP=$(<font color="#ffffff">./2.cluster_lb_externalip.sh</font>&nbsp;)
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ echo &quot;THE LB IP is $LB_IP&quot;
    </p>
    <p>
      THE LB IP is <font color="#00b6c6">172.18.0.2</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_618084293" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      And we can &quot;stop&quot; the cluster (which releases its used memory).<b>.</b>.
    </p>
</div>
<div ID="freeplaneNode__ID_1536875957" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">./2.cluster_stop.sh</font>
    </p>
    <p>
      ████████████████████████./2.cluster_stop.sh@[15]&gt;&nbsp;&nbsp;k3d cluster stop 
      myk3dcluster
    </p>
    <p>
      INFO[0000] Stopping cluster 'myk3dcluster'
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$
    </p>
    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">free -h</font>
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;used&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;free&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shared&nbsp;&nbsp;buff/cache&nbsp;&nbsp;&nbsp;available
    </p>
    <p>
      Mem:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.8Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#00b6c6">413Mi</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.0Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.0Mi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.1Gi
    </p>
    <p>
      Swap:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1262849277" class=FreeplaneNodeGen>
    <p>
      .<b>.</b>. and another moment &quot;start&quot; it again
    </p>
</div>
<div ID="freeplaneNode__ID_1441818241" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">./2.cluster_start.sh</font>
    </p>
    <p>
      ████████████████████████./2.cluster_start.sh@[15]&gt;&nbsp;&nbsp;k3d cluster start 
      myk3dcluster
    </p>
    <p>
      INFO[0000] Starting cluster 'myk3dcluster'
    </p>
    <p>
      INFO[0000] Starting servers...
    </p>
    <p>
      INFO[0000] Starting Node 'k3d-myk3dcluster-server-0'
    </p>
    <p>
      INFO[0005] Starting agents...
    </p>
    <p>
      INFO[0005] Starting helpers...
    </p>
    <p>
      INFO[0005] Starting Node 'k3d-myk3dcluster-serverlb'
    </p>
    <p>
      ████████████████████████./2.cluster_start.sh@[16]&gt;&nbsp;&nbsp;./2.cluster_stats.sh
    </p>
    <p>
      NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SERVERS&nbsp;&nbsp;&nbsp;AGENTS&nbsp;&nbsp;&nbsp;LOADBALANCER
    </p>
    <p>
      myk3dcluster&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;true
    </p>
    <p>
      NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ROLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;STATUS
    </p>
    <p>
      k3d-myk3dcluster-server-0&nbsp;&nbsp;&nbsp;server&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;myk3dcluster&nbsp;&nbsp;&nbsp;running
    </p>
    <p>
      k3d-myk3dcluster-serverlb&nbsp;&nbsp;&nbsp;loadbalancer&nbsp;&nbsp;&nbsp;myk3dcluster&nbsp;&nbsp;&nbsp;running
    </p>
    <p>
      CONTAINER ID&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CPU %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEM USAGE / 
      LIMIT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MEM %&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NET I/O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BLOCK I/O&nbsp;&nbsp;&nbsp;&nbsp;PIDS
    </p>
    <p>
      dd5faf320669&nbsp;&nbsp;&nbsp;host-local-mirror-registry&nbsp;&nbsp;&nbsp;0.00%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;36.31MiB / 
      7.775GiB&nbsp;&nbsp;&nbsp;0.46%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;347MB / 174MB&nbsp;&nbsp;&nbsp;0B / 173MB&nbsp;&nbsp;&nbsp;12
    </p>
    <p>
      6f2c332e39fb&nbsp;&nbsp;&nbsp;k3d-myk3dcluster-serverlb&nbsp;&nbsp;&nbsp;&nbsp;0.00%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.336MiB / 
      7.775GiB&nbsp;&nbsp;&nbsp;0.04%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;516B / 0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B / 0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5
    </p>
    <p>
      9db0a4af2982&nbsp;&nbsp;&nbsp;k3d-myk3dcluster-server-0&nbsp;&nbsp;&nbsp;&nbsp;13.61%&nbsp;&nbsp;&nbsp;&nbsp;512.7MiB / 
      7.775GiB&nbsp;&nbsp;&nbsp;6.44%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;628B / 1.6kB&nbsp;&nbsp;&nbsp;&nbsp;0B / 406kB&nbsp;&nbsp;&nbsp;25
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER-IP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXTERNAL-IP&nbsp;&nbsp;&nbsp;PORT(S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kubernetes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;kube-dns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;53/UDP,53/TCP,9153/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;metrics-server&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.255.69&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LoadBalancer&nbsp;&nbsp;&nbsp;10.43.113.182&nbsp;&nbsp;&nbsp;172.18.0.2&nbsp;&nbsp;&nbsp;&nbsp;80:30219/TCP,443:30420/TCP&nbsp;&nbsp;&nbsp;25m
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1078830009" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">free -h</font>
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;used&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;free&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shared&nbsp;&nbsp;buff/cache&nbsp;&nbsp;&nbsp;available
    </p>
    <p>
      Mem:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.8Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#00b6c6">1.1Gi</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.4Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.0Mi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3Gi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.4Gi
    </p>
    <p>
      Swap:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0B
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_228758620" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      uzer@vm-client:~/k3d-lab$ <font color="#ffffff">kubectl get all -A</font>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RESTARTS&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/helm-install-traefik-crd-qm97l&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/helm-install-traefik-79kcc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/metrics-server-86cbb8457f-hhgfq&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/coredns-7448499f4d-qkh4g&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/local-path-provisioner-5ff76fc89d-q9q8h&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/svclb-traefik-r6vx6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2/2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pod/nginx2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9m10s
    </p>
    <p>
      default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pod/nginx&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;pod/traefik-97b44b794-cdvb8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Running&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TYPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLUSTER-IP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EXTERNAL-IP&nbsp;&nbsp;&nbsp;PORT(S)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      default&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;service/kubernetes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;service/kube-dns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.0.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;53/UDP,53/TCP,9153/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;service/metrics-server&nbsp;&nbsp;&nbsp;ClusterIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.43.255.69&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;443/TCP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;service/traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LoadBalancer&nbsp;&nbsp;&nbsp;10.43.113.182&nbsp;&nbsp;&nbsp;172.18.0.2&nbsp;&nbsp;&nbsp;&nbsp;80:30219/TCP,443:30420/TCP&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DESIRED&nbsp;&nbsp;&nbsp;CURRENT&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;UP-TO-DATE&nbsp;&nbsp;&nbsp;AVAILABLE&nbsp;&nbsp;&nbsp;NODE 
      SELECTOR&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;daemonset.apps/svclb-traefik&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;none&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;UP-TO-DATE&nbsp;&nbsp;&nbsp;AVAILABLE&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/metrics-server&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/local-path-provisioner&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/coredns&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;deployment.apps/traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DESIRED&nbsp;&nbsp;&nbsp;CURRENT&nbsp;&nbsp;&nbsp;READY&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/metrics-server-86cbb8457f&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/local-path-provisioner-5ff76fc89d&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/coredns-7448499f4d&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;replicaset.apps/traefik-97b44b794&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
<br>
    </p>
    <p>
      NAMESPACE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPLETIONS&nbsp;&nbsp;&nbsp;DURATION&nbsp;&nbsp;&nbsp;AGE
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;job.batch/helm-install-traefik-crd&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;22s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
    <p>
      kube-system&nbsp;&nbsp;&nbsp;job.batch/helm-install-traefik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25s&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26m
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1403649961" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      There are some other files in the repo around istio and manifests... 
      those can probably still work or need some caring to update them <b>;) </b>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      Hope this was usefull :) happy hacking ;)
    </p>
</div>
</div>
