---
title: "podlogreader - a GoLang K8s Custom-controller"
date: 2021-08-11


draft: false

categories:
#- Infosec
- Devops
- Sysadmin
- Cloud
- K8s
#- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
- cve
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
#- bash
#- ansible
#- quickref
- k8s
- k3s
- k3d
---
<div>
<div ID="freeplaneNode__ID_845132160" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">podlogreader</font></b><br><br><a href="https://github.com/zipizap/podlogreader">https://github.com/zipizap/podlogreader </a>
    </p>
    <p>
<br>
    </p>
    <p>
      A k8s-controller that makes possible for a serviceaccount to read logs 
      from specific deployment-pods (and no other deployments or pods)
    </p>
    <p>
<br>
    </p>
    <p>
      For a deployment to use it, it should add the label 
      &quot;podlogreader-affiliate: enable&quot; in its pods-spec.
    </p>
    <p>
<br>
    </p>
    <p>
      It will create in the namespace a serviceaccount, rolebinding and role 
      with minimal permitions to read the logs of only those pods of that 
      deployment. It will then keep always in-sync that role with any 
      deployment-pods changes.
    </p>
    <p>
<br>
    </p>
    <p>
      So the overall effect is to have a serviceaccount, for that deployment, 
      that can read deployment-pods logs (and only those of the deployment, no 
      other!), and which is resilient to deployment pod changes (replicas 
      increased/decresed, pods created, etc...)
    </p>
    <p>
<br>
    </p>
    <p>
      In all honesty, this was a successfull intent to try and see if this 
      whole idea was even possible to be accomplished :)
    </p>
    <p>
<br>
    </p>
    <p>
      The code is very simple, and all runs in an independent process without 
      interference with other internal kubernetes control-loops or controllers 
      (ie, its uncomplicated, self-contained, and looks safe towards overall 
      cluster operation :) )
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      See more details, installation instructions and links in git-repo&nbsp; <a href="https://github.com/zipizap/podlogreader">https://github.com/zipizap/podlogreader 
      </a>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
</div>
<div ID="freeplaneNode__ID_1264146659" class=FreeplaneNodeGen>
    <p>
      <b>
</b>    </p>
    <p>
      <b>
</b>    </p>
    <p>
      <b>:)</b>
    </p>
    <p>
      I had a blast learning and doing all this, during Christmas vacations of 
      2020
    </p>
</div>
</div>
