---
title: "LXD 2 install via snap"
date: 2019-12-27 


draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- 4hackers
#- 4nonhackers
- informative
- lxd
- lxc
#- exploitation
#- suid
#- etc
#- portugues
#- sociology

---
<div>
<div ID="freeplaneNode__ID_1419460927" class=FreeplaneNodeGen>
    <p>
      LXD is sponsored by Canonical and available by default in Ubuntu-18.04 as a deb package.<br>However the most up-to-date distribution of LXD is made via <b>snap</b>, which also helps to install LXD in any other Linux distro (Centos, etc)<br><br>In case <b>snap </b>is<b>&#160;</b>new to you, take 5min to read about it - its an extremely interesting technology to manage and run packages on any linux distro (distro agnostic), with greater focus on security by resticting capabilities on the running binaries (involving apparmour profiles and etc). Also the binaries are stored in a somewhat compresse-tamper-proof-resistant format - all good things :) Also, snap takes care of checking and updating each snap-package 4 times a day, straight from the developer - and that never gave me any problem.
    </p>
    <p>
      <br>
      So, installing LXD via snap will give us the latest version and take care of automatic updates.<br>
    </p>
    <p>
      Lets install LXD via snap on ubuntu18.04, for which we will need to remove the LXD-deb package and then install and configure the LXD-snap binary :)<br><br><font size="1">NOTE: lxd is preinstalled via deb in ubuntu-server-16.04 and 18.04, and this procedure applies to these cases. In ubuntu-server-18.10 and newer ubuntus, the lxd comes preinstalled as a <i><u>snap</u></i>, so you can jump directly to &quot;lxd init&quot; as there is no need to &quot;lxd.migrate&quot;&#160;(more info <a href="https://ubuntu.com/blog/lxd-in-4-easy-steps">in this footnote</a>)<br size="1"></font>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
      <b><font size="4">LXD install via snap, instead of deb, in ub18.04-server</font></b><br><br>Without more delays, lets jump to the shell.
    </p>
    <p>
      This was done with Ubuntu-18.04 Server. You can try it yourself in a Virtualbox/VmWare VM with ubuntu-18.04-server installed
    </p>
</div>
<div ID="freeplaneNode__ID_1686178786" class=FreeplaneNodeGen>
<p>Switch to the normal-user "ubuntu" in this example</p></div>
<div ID="freeplaneNode__ID_695757762" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      # as user <b>root</b>:
    </p>
    <p>
      su - ubuntu
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_520509119" class=FreeplaneNodeGen>
    <p>
      Now as user <b>ubuntu</b>
    </p>
</div>
<div ID="freeplaneNode__ID_1153991583" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>sudo snap install lxd</p></div>
</div>
<div ID="freeplaneNode__ID_1512476866" class=FreeplaneNodeGen>
    <p>
      This will migrate from <u>deb-lxd</u>&#160;to <u>snap-lxd</u>
    </p>
</div>
<div ID="freeplaneNode__ID_1870290285" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      sudo lxd.migrate
    </p>
    <p>
      &#160;&#160;# Do you want to uninstall the old LXD (yes/no) [default=no]? <font color="#ffffff">yes</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1455116103" class=FreeplaneNodeGen>
<p>Now logout and re-login (to assure to reload the new group "lxd" into environment of user "ubuntu")</p></div>
<div ID="freeplaneNode__ID_1632648000" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>exit</p></div>
</div>
<div ID="freeplaneNode__ID_669384012" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>su - ubuntu</p></div>
</div>
<div ID="freeplaneNode__ID_137385514" class=FreeplaneNodeGen>
<p>At this point, we can check that now the lxc binary found in the path is the one installed via snap, like "/snap/bin/lxc"</p></div>
<div ID="freeplaneNode__ID_276893698" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>which lxc</p></div>
</div>
<div ID="freeplaneNode__ID_1138639628" class=FreeplaneNodeGen>
    <p>
      <font size="1">NOTE: You could avoid the &quot;lxd.migrate&quot;&#160;&#160;and alternativelly make a manual &quot;<i>sudo apt remove --purge lxd lxd-client</i>&quot; of the original lxd-deb installation, and then afterwards install the lxd-snap (more details in <a href="https://github.com/lxc/lxd/issues/3922">this blog</a>)</font>
    </p>
</div>
<div ID="freeplaneNode__ID_306143903" class=FreeplaneNodeGen>
<p>In this step, we will execute "lxd init", using the lxd-snap:</p></div>
<div ID="freeplaneNode__ID_423639572" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      ubuntu@lxchosta:~$ <font color="#ffffff">lxd init</font>
    </p>
    <p>
      Would you like to use LXD clustering? (yes/no) [default=no]:
    </p>
    <p>
      Do you want to configure a new storage pool? (yes/no) [default=yes]:
    </p>
    <p>
      Name of the new storage pool [default=default]:
    </p>
    <p>
      Name of the storage backend to use (btrfs, ceph, dir, lvm, zfs) [default=zfs]:
    </p>
    <p>
      Create a new ZFS pool? (yes/no) [default=yes]:
    </p>
    <p>
      Would you like to use an existing block device? (yes/no) [default=no]:
    </p>
    <p>
      Size in GB of the new loop device (1GB minimum) [default=15GB]:
    </p>
    <p>
      Would you like to connect to a MAAS server? (yes/no) [default=no]:
    </p>
    <p>
      Would you like to create a new local network bridge? (yes/no) [default=yes]:
    </p>
    <p>
      What should the new bridge be called? [default=lxdbr0]:
    </p>
    <p>
      What IPv4 address should be used? (CIDR subnet notation, &#8220;auto&#8221; or &#8220;none&#8221;) [default=auto]:
    </p>
    <p>
      What IPv6 address should be used? (CIDR subnet notation, &#8220;auto&#8221; or &#8220;none&#8221;) [default=auto]:
    </p>
    <p>
      Would you like LXD to be available over the network? (yes/no) [default=no]:
    </p>
    <p>
      Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
    </p>
    <p>
      Would you like a YAML &quot;lxd init&quot; preseed to be printed? (yes/no) [default=no]:
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1671033482" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Create the first new container</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_944513823" class=FreeplaneNodeGen>
    <p>
      Lets now&#160;&#160;create a test-container called &quot;<b>myContainer3</b>&quot; from the <b>image ubuntu-minimal </b>
    </p>
    <p>
      The ubuntu-minimal image is a cloud-enhanced version of ubuntu18.04-server, optimized towards container utilization).
    </p>
    <p>
      These commands will do a sequence of internal steps to achieve the final result, such as adding a new <b>remote-image-repository</b>, then searching and downloading the <b><i>image</i></b>&#160;of ubuntu-18.04-minimal (architecture amd64 which is the default) to <b>local image cache</b>, and then creating a <b>new container </b>from that local-image
    </p>
</div>
<div ID="freeplaneNode__ID_1776168615" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc remote add --protocol simplestreams ubuntu-minimal https://cloud-images.ubuntu.com/minimal/releases/</p></div>
</div>
<div ID="freeplaneNode__ID_511828529" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc launch ubuntu-minimal:bionic myContainer3</p></div>
</div>
<div ID="freeplaneNode__ID_1754971098" class=FreeplaneNodeGen>
    <p>
      In the end, the container &quot;myContainer3&quot; is left up and running.
    </p>
    <p>
      By default it will have a <b>dynamic-ip </b>assigned by internal-dhcp (a <u>dnsmasq </u>managed by lxd, executing on the bridge-gateway-ip). The ips are inside a local-bridge subnet that has its traffic dNAT'ed and routed via the bridge-interface-of-the-host.
    </p>
    <p>
      The ports exposed by the container are inside the bridge-subner and not reacheable from other external-networks or public-internet - only to the local-lxd-host as he is part of that same bridge. Its possible to &quot;map&quot; a container-port to a host-port in a public-internet-ip, with some additional lxd commands (if you do it, dont forget to review the host firewall accordingly)
    </p>
</div>
<div ID="freeplaneNode__ID_1784724370" class=FreeplaneNodeGen>
<p>---</p></div>
<div ID="freeplaneNode__ID_1914705044" class=FreeplaneNodeGen>
    <p>
      Now lets execute in <b>myContainer3</b>&#160;an interactive bash shell<br><font size="1">NOTE: what really is happening here behind the curtain, is that a new bash-process is launched inside the kernel-namespaces (fs, net, proc, users, etc...) that are assigned to the <b>myContainer3</b>. And that namespace-separation is what creates the ilusion of running in an isolated environment similar to a VM. Do keep in mind, that its always one-and-only-common-kernel that is shared between all containers and also the host. But a very polished ilusion, indistinguisheable for most practical effects, and with a much lighter footprint of mem/cpu/fs compared to a real-VM. Its a linux-kernel-container.</font>
    </p>
</div>
<div ID="freeplaneNode__ID_547109504" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      ubuntu@lxchosta:~$ <font color="#ffffff">lxc exec </font><font color="#9999ff">myContainer3</font><font color="#ffffff">&#160;-- /bin/bash</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_715861150" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      root@<font color="#9999ff">myContainer3</font>:~# <font color="#ffffff">ps -ef --forest</font>
    </p>
    <p>
      UID&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;PID&#160;&#160;PPID&#160;&#160;C STIME TTY&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;TIME CMD
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;254&#160;&#160;&#160;&#160;&#160;0&#160;&#160;0 01:01 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /bin/bash
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;261&#160;&#160;&#160;254&#160;&#160;0 01:02 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00&#160;&#160;\_ ps -ef --forest
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;1&#160;&#160;&#160;&#160;&#160;0&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /sbin/init
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;60&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /lib/systemd/systemd-journald
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;63&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /lib/systemd/systemd-udevd
    </p>
    <p>
      systemd+&#160;&#160;&#160;122&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /lib/systemd/systemd-networkd
    </p>
    <p>
      systemd+&#160;&#160;&#160;124&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /lib/systemd/systemd-resolved
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;158&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /usr/sbin/cron -f
    </p>
    <p>
      daemon&#160;&#160;&#160;&#160;&#160;160&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /usr/sbin/atd -f
    </p>
    <p>
      message+&#160;&#160;&#160;164&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;168&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /usr/bin/python3 /usr/bin/networkd-dispatcher --run-startup-triggers
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;169&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /lib/systemd/systemd-logind
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;171&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 console&#160;&#160;00:00:00 /sbin/agetty -o -p -- \u --noclear --keep-baud console 115200,38400,9600 linux
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;179&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /usr/sbin/sshd -D
    </p>
    <p>
      root&#160;&#160;&#160;&#160;&#160;&#160;&#160;180&#160;&#160;&#160;&#160;&#160;1&#160;&#160;0 00:54 ?&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;00:00:00 /usr/bin/python3 /usr/share/unattended-upgrades/unattended-upgrade-shutdown --wait-for-signal
    </p>
    <p>
      root@myContainer3:~#
    </p>
    <p>
      root@myContainer3:~# <font color="#ffffff">route -n</font>
    </p>
    <p>
      Kernel IP routing table
    </p>
    <p>
      Destination&#160;&#160;&#160;&#160;&#160;Gateway&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Genmask&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;Flags Metric Ref&#160;&#160;&#160;&#160;Use Iface
    </p>
    <p>
      0.0.0.0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<font color="#ffffff">10.37.204.1</font>&#160;&#160;&#160;&#160;&#160;0.0.0.0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;<font color="#ffffff">UG</font>&#160;&#160;&#160;&#160;100&#160;&#160;&#160;&#160;0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;0 eth0
    </p>
    <p>
      10.37.204.0&#160;&#160;&#160;&#160;&#160;0.0.0.0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;255.255.255.0&#160;&#160;&#160;U&#160;&#160;&#160;&#160;&#160;0&#160;&#160;&#160;&#160;&#160;&#160;0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;0 eth0
    </p>
    <p>
      10.37.204.1&#160;&#160;&#160;&#160;&#160;0.0.0.0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;255.255.255.255 UH&#160;&#160;&#160;&#160;100&#160;&#160;&#160;&#160;0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;0 eth0
    </p>
    <p>
      root@myContainer3:~# <font color="#ffffff">ip add s</font>
    </p>
    <p>
      1: lo: &lt;LOOPBACK,UP,LOWER_UP&gt; mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    </p>
    <p>
      &#160;&#160;&#160;&#160;link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    </p>
    <p>
      &#160;&#160;&#160;&#160;inet 127.0.0.1/8 scope host lo
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;valid_lft forever preferred_lft forever
    </p>
    <p>
      &#160;&#160;&#160;&#160;inet6 ::1/128 scope host
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;valid_lft forever preferred_lft forever
    </p>
    <p>
      5: <font color="#ffffff">eth0</font>@if6: &lt;BROADCAST,MULTICAST,UP,LOWER_UP&gt; mtu 1500 qdisc noqueue state UP group default qlen 1000
    </p>
    <p>
      &#160;&#160;&#160;&#160;link/ether 00:16:3e:2d:4b:98 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    </p>
    <p>
      &#160;&#160;&#160;&#160;inet <font color="#ffffff">10.37.204.236/24</font>&#160;brd 10.37.204.255 scope global dynamic eth0
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;valid_lft 2993sec preferred_lft 2993sec
    </p>
    <p>
      &#160;&#160;&#160;&#160;inet6 fd42:fac:3bb4:3213:216:3eff:fe2d:4b98/64 scope global dynamic mngtmpaddr noprefixroute
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;valid_lft 3554sec preferred_lft 3554sec
    </p>
    <p>
      &#160;&#160;&#160;&#160;inet6 fe80::216:3eff:fe2d:4b98/64 scope link
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;valid_lft forever preferred_lft forever
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      root@myContainer3:~# <font color="#ffffff">exit</font>
    </p>
    <p>
      exit
    </p>
    <p>
      ubuntu@lxchosta:~$
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1603290474" class=FreeplaneNodeGen>
<p>---</p></div>
<div ID="freeplaneNode__ID_841284741" class=FreeplaneNodeGen>
<p>Finally, back in the lxc-host, we can see the list of containers running with their ips</p></div>
<div ID="freeplaneNode__ID_1182749976" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      ubuntu@lxchosta:~$ <font color="#ffffff">lxc list</font>
    </p>
    <p>
      +--------------+---------+----------------------+----------------------------------------------+------------+-----------+
    </p>
    <p>
      |&#160;&#160;&#160;&#160;&#160;NAME&#160;&#160;&#160;&#160;&#160;|&#160;&#160;STATE&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;IPV4&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;IPV6&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;|&#160;&#160;&#160;&#160;TYPE&#160;&#160;&#160;&#160;| SNAPSHOTS |
    </p>
    <p>
      +--------------+---------+----------------------+----------------------------------------------+------------+-----------+
    </p>
    <p>
      | <font color="#ffffff">myContainer3</font>&#160;| <font color="#ffffff">RUNNING </font>| <font color="#ffffff">10.37.204.236</font>&#160;(eth0) | fd42:fac:3bb4:3213:216:3eff:fe2d:4b98 (eth0) | PERSISTENT | 0&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;|
    </p>
    <p>
      +--------------+---------+----------------------+----------------------------------------------+------------+-----------+
    </p>
    <p>
      ub@lxchosta:~$
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1036009611" class=FreeplaneNodeGen>
<p>And we can stop it and start it again latter</p></div>
<div ID="freeplaneNode__ID_967191645" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc list</p>
<p>lxc stop --force myContainer3</p>
<p>lxc start myContainer3</p>
<p>lxc list</p></div>
</div>
<div ID="freeplaneNode__ID_22189631" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">References</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1354164017" class=FreeplaneNodeGen>
    <p>
      <a href="https://blog.simos.info/how-to-migrate-lxd-from-deb-ppa-package-to-snap-package/">https://blog.simos.info/how-to-migrate-lxd-from-deb-ppa-package-to-snap-package/</a>
    </p>
</div>
</div>
