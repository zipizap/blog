---
title: "nginx fake one-line rce poc or how curl can trick back"
date: 2019-06-03 


draft: false

categories: 
- Infosec
#- Devops
#- Sysadmin
#- Cloud
#- Programming

tags:
#- 4hackers
- 4nonhackers
- informative
#- exploitation
#- suid
#- etc
- curl

---
<div>
<div ID="freeplaneNode__ID_836498002" class=FreeplaneNodeGen>
    <p>
      Yesterday a curious RCE was twitted, and at first sight it seemed some kind of url command-injection. But it was just a prank joke, and used some curl trickery to make a request from a webpage that the user is not aware of.
    </p>
    <p>
<br>
    </p>
    <p>
      The curl <i>trickery </i>is interesting and not that easy to see at first sight - here is a bit of it breaked-down-apart
    </p>
</div>
<div ID="freeplaneNode__ID_1423801094" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_747629234481949682.png"
    link="png_747629234481949682.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1361442068" class=FreeplaneNodeGen>
    <p>
      Latter found out that original author has explained it here: <a href="https://medium.com/@notdan/curl-slight-of-hand-exploit-hysteria-29a82e5851d">https://medium.com/@notdan/curl-slight-of-hand-exploit-hysteria-29a82e5851d</a>
    </p>
</div>
</div>
