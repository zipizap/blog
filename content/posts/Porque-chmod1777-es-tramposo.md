---
title: "Porque chmod1777 es tramposo"
date: 2019-10-07 


draft: false

categories: 
- Infosec
#- Devops
- Sysadmin
#- Cloud
- Programming

tags:
- 4hackers
- 4nonhackers
#- informative
#- exploitation
#- suid
#- etc

---
<div>
<div ID="freeplaneNode__ID_1423074868" class=FreeplaneNodeGen>
    <p>
      El <b><font color="#ffff00">1</font></b>&#160;del <b><font color="#ffff00">1</font></b>777 cambia el comportamiento <i><u>dentro del directorio</u></i>, es como si al directorio le <u><i>quitara algunos permisos del &quot;other users&quot;</i></u>.<br><br>
    </p>
    <p>
      Por ejemplo, dentro de un directorio con permisos 1777, hay un fichero de &quot;alice&quot;, para el cual el &quot;other-users&quot; es el usuario &quot;bob&quot;:
    </p>
    <p>
      <br>
      &#160;&#160;- &quot;bob&quot; puede <b>listar </b>los ficheros de &quot;alice&quot;. Como <b>o+r&#160;en el directorio</b><br>&#160;&#160;- &quot;bob&quot; no puede <b>renombrar</b>, ni <b>borrar</b>&#160;&#160;ficheros de &quot;alice&quot; . Como <b>o-w&#160;en el directorio</b><br>&#160;&#160;- &quot;bob&quot; no puede <b>escribir</b>&#160;pero puede <b>ler</b>&#160;ficheros de &quot;alice&quot;. Se respecta el r-- del fichero de alice, o sea, se respetan los permisos de los ficheros dentro del directorio.<br><br>
    </p>
    <p>
      Hasta aqu&#237;, todo ventajas :)
    </p>
</div>
<div ID="freeplaneNode__ID_1352320997" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_2605548027873463506.png"
    link="png_2605548027873463506.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_947939030" class=FreeplaneNodeGen>
    <p>
      .
    </p>
    <p>
<br>
    </p>
    <p>
      Pero &quot;bob&quot; puee <font color="#ff6666"><b>crear</b></font>&#160;sus nuevos ficheros dentro del directorio, lo que <b><font color="#ff6666">suena muy mal</font></b>...
    </p>
    <p>
      <br>
      Si alguna vez, al instalar/configurar un nuevo <b><font color="#cc99ff"><i>servicio-expuesto </i></font></b>(apache, nginx, u otro servicio), uno estuviera dudando de los permisos y digamos probara a dejar <b><font color="#ff6666">1777 algun directorio publico&#160;del servicio </font></b>(por olvido, por las prisas, etc), entonces un usuario malicioso podr&#237;a aprovechar para, durante un ataque, lograr crear nuevos archivos en ese directorio, como webshells, html malicioso, etc...
    </p>
    <p>
<br>
    </p>
    <p>
      Es decir, si no hubiera servicio, no habr&#237;a peligro utilizar 1777, pero si por acidente alg&#250;n d&#237;a esto ocurre con algun servicio-expuesto, entonces el 1777 se vuelve inmediatamente muy muy peligroso, puede ser combinado con otras oportunidades de ataque... un web-root con subdirectorios 1777 es un desaf&#237;o al desastre... es por eso que <b><font color="#ff6666">suena muy mal</font></b>...<br><br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      Por ejemplo, una vez ataqu&#233; una maquina de HackTheBox, que ten&#237;a un servidor web, donde hab&#237;a una SqlInjection, y donde tambi&#233;n el web-root ten&#237;a permisos demasiado relajados que permit&#237;a a cualquier-usuario crear nuevos ficheros.
    </p>
    <p>
<br>
    </p>
    <p>
      Con la vulnerabilidad de SQL-injection, lo m&#225;s da&#241;ino que parec&#237;a ser posible lograr, era justamente crear nuevos ficheros locales (tambien se pod&#237;a ver todas las bbdds, pero esas no parec&#237;an tener nada &#250;til que ayudara al acceso remoto al sistema operativo).
    </p>
    <p>
<br>
    </p>
    <p>
      Con el SQL-injection se pod&#237;a crear nuevos archivos locales en el servidor, pero lo hac&#237;a con los permisos del usuario del servicio-sql, que era un usuario distinto al usuario del servicio-web (cosa bien hecha, sino podr&#237;a ser todo mucho mas f&#225;cil - eso estaba bien).
    </p>
    <p>
<br>
    </p>
    <p>
      Pero el usuario del servicio-web hab&#237;a dejado mal-configurado los permisos del web-root, que permit&#237;an a cualquier usuario crear nuevos archivos en el web-root. Debido a ello, con el SQL-injection pude crear archivos en ese web-root mal-configurado, y as&#237; he creado una webshell, que despu&#233;s he logrado que fuera ejecutada por el servidor-web, y cuyo resultado fue facilitar una remote-shell al servidor (acceso remoto por shell)<br><br>O&#160;sea, en su conjunto - la vulnerabilidad SQL-injection, combinada con los permisos del SQL de crear ficheros locales, combinado con los permisos relajados del web-root mal-configurado - en su conjunto han permitido lograr acceso remoto, una shell remota al servidor.<br><br>Creo que sin ese web-root relajado talvez no hubiera podido ir mas all&#225; de ver las bbdds, y dudo si pudiera llegar a lograr, el acceso remoto por shell.&#160;&#160;Una vez logrado el acceso-remoto por shell, el resto se vuelve mas facil: establecer persistencia, escalar privilegios hac&#237;a root, etc... una vez dentro, suele haber mucho mas por donde atacar, hasta probablemente lograr permisos root, hacerse con el control de esa maquina, y entonces pivotear desde ella para seguir atacando el resto de la red interna.
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      2019/10/07 - Update:
    </p>
    <p>
<br>
    </p>
    <p>
      De la discusi&#243;n y feedback generado en telegram, mencionar que el <b><font color="#ffff00">1</font></b>&#160; se llama <b><i><font color="#ffff00">sticky-bit </font></i></b>o <b><font color="#ffff00">restricted deletion flag </font></b>- para mas informaci&#243;n ver &quot;man chmod&quot;
    </p>
    <p>
<br>
    </p>
    <p>
      <a href="https://0x30.io/">EA1HET(Jonathan)</a>&#160;ha compartido el art&#237;culo &#160;<a href="https://web.archive.org/web/20120203044307/http://content.hccfl.edu/pollock/AUnix1/FilePermissions.htm">Unix File and Directory Permissions and Modes</a><i>&#160;- &quot;especialmente la secci&#243;n de Usage&quot;. </i>Una buena lectura para entender los permisos unix
    </p>
</div>
</div>
