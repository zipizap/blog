---
title: "Testing draft"
date: 2019-03-27


draft: true

categories: 
- Infosec
#- Devops
#- Sysadmin
#- Cloud
#- Programming

tags:
- exploitation
- suid
#- etc

---
<div>
<div ID="freeplaneNode__ID_1276799965" class=FreeplaneNodeGen>
<p>simple-node without formatting</p>
<p>&nbsp;&nbsp;- no image</p></div>
<div ID="freeplaneNode__ID_1036066912" class=FreeplaneNodeGen>
    <p>
      simple-node with <b>formatting</b>
    </p>
    <p>
      &#160;&#160;- no image
    </p>
</div>
<div ID="freeplaneNode__ID_1020477105" class=FreeplaneNodeGen>
{{< highlight c "linenos=table" >}}
codeNodeWithoutFormatting
  - without formatting
  + with-or-without attribute "language"
{{< /highlight >}}
</div>
<div ID="freeplaneNode__ID_1297496677" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      codeNodeWithFormatting <b><font color="#ff3333">formatting</font></b>
    </p>
    <p>
      <b><font color="#0000c0">line 2 </font></b>
    </p>
    <p>
      <b><font color="#0000c0">&#160;</font></b>
    </p>
    <p>
      <b><font color="#ff3333">line 4</font></b>
    </p>
</div>
</div>
</div>
