---
title: "ANSIBLE - quickref diagram of files layout"
date: 2020-01-20 


draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- ansible
#- 4hackers
#- 4nonhackers
#- informative
#- lxd
#- lxc
#- exploitation
#- suid
#- etc
#- portugues
#- sociology
- quickref

---
<div>
<div ID="freeplaneNode__ID_466354279" class=FreeplaneNodeGen>
    <p>
      <b>Ansible</b> - empower yourself to make reliable (idempotent) 
      configuration changes at many hosts, with ease and peace-of-mind.<br><br>Sharing 
      here this diagram that I've build myself when starting with ansible a 
      couple years ago - hope its usefull :)
    </p>
</div>
<div ID="freeplaneNode__ID_656331505" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_3664736965786817092.png"
    link="png_3664736965786817092.png"
    width="600"
>}}
</div>
</div>
