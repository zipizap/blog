---
title: "Kubernetes quick-lab with kubeadm - 3node-cluster with Weave CNI"
date: 2021-12-02


draft: false

categories:
#- Infosec
- Devops
- Sysadmin
- Cloud
- K8s
#- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
#- cve
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
#- bash
#- ansible
#- quickref
- k8s
- k3s
- k3d
---
<div>
<div ID="freeplaneNode__ID_603701596" class=FreeplaneNodeGen>
    <p>
      While stuying for the Certified Kubernetes Administrator (CKA) exam, 
      following Mumshad course in Udemy, I've forked and added some usefull 
      scripts to be able to quickly create a new cluster with kubeadm<br><br>So 
      when I want to test something with a kubeadm-deployed cluster, with just 
      running some scripts in &lt;5min I can have a 3-node-cluster with weave 
      CNI, ready for kubectl and testing ;)
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
      <a href="https://github.com/zipizap/certified-kubernetes-administrator-course">https://github.com/zipizap/certified-kubernetes-administrator-course </a>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      Happy hacking :)
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      Props to Mumshad for all its good work ;)
    </p>
</div>
</div>
