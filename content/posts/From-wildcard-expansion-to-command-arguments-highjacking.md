---
title: "From wildcard expansion to command arguments highjacking"
date: 2019-06-01 


draft: false

categories: 
- Infosec
#- Devops
- Sysadmin
#- Cloud
- Programming

tags:
- technical
- informative
- privilegeEscalation
- wildcardExpansion
#- suid
#- etc

---
<div>
<div ID="freeplaneNode__ID_1095638273" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_4500740237486432791.png"
    link="png_4500740237486432791.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1896473069" class=FreeplaneNodeGen>
    <p>
      This post is a bit technical
    </p>
    <p>
<br>
    </p>
    <p>
      If you're not too technical just get the idea that its possible to misguide typical linux-administration-commands and shell-scripts, to leverage system compromise.
    </p>
    <p>
<br>
    </p>
    <p>
      Yes, it looks like a real <i>troublemaker</i>&#160;- something that wont get &quot;fixed&quot; any time soon, as it lies in the programming source-code-base of each command, and only seems preventable via security-focused code reviews. So this is something with a very extended effect and probably very-long-term lifespan.
    </p>
    <p>
<br>
    </p>
    <p>
      That being said, let's go straight to the shell
    </p>
</div>
<div ID="freeplaneNode__ID_1380999048" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <div bgcolor="#333333" color="#808080">
      <pre face="Courier New" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">touch a</font>
ub@seclab:/tmp/d$ <font color="#ffffff">touch b</font>
ub@seclab:/tmp/d$ <font color="#ffff00">touch &quot;--version&quot;                       &lt;&lt; not --version but ./--version</font>
touch (GNU coreutils) 8.28
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later &lt;http://gnu.org/licenses/gpl.html&gt;.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Paul Rubin, Arnold Robbins, Jim Kingdon,
David MacKenzie, and Randy Smith.
ub@seclab:/tmp/d$ <font color="#ffff00">touch ./--version</font>
ub@seclab:/tmp/d$
ub@seclab:/tmp/d$ <font color="#ffffff">ll</font>
total 8
drwxrwxr-x  2 ub   ub   4096 Apr 26 23:44 ./
drwxrwxrwt 13 root root 4096 Apr 26 23:44 ../
-rw-rw-r--  1 ub   ub      0 Apr 26 23:44 a
-rw-rw-r--  1 ub   ub      0 Apr 26 23:44 b
<font color="#ffff00">-rw-rw-r--  1 ub   ub      0 Apr 26 23:44 --version</font>
ub@seclab:/tmp/d$
ub@seclab:/tmp/d$ <font color="#ffffff">echo ls *&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&lt;&lt; lets see shell wildcard expansion: what arguments will the command really get after the shell wildcard expansion</font><br>ls a b <font color="#ffff00">--version</font>
ub@seclab:/tmp/d$<font color="#ff6666">&#160;</font><font color="#ff9999">ls *&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&lt;&lt; [2] </font>
<font color="#ff3333">ls (GNU coreutils) 8.28
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later &lt;http://gnu.org/licenses/gpl.html&gt;.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Richard M. Stallman and David MacKenzie.</font>
ub@seclab:/tmp/d$
ub@seclab:/tmp/d$ <font color="#ff9999">rm *                            &lt;&lt; [2]</font>
<font color="#ff3333">rm (GNU coreutils) 8.28
Copyright (C) 2017 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later &lt;http://gnu.org/licenses/gpl.html&gt;.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Paul Rubin, David MacKenzie, Richard M. Stallman,
and Jim Meyering.<br color="#ff3333">
</font>ub@seclab:/tmp/d$ <font color="#ffff00">rm ./--help                   &lt;&lt; prepend filename with relative-or-fullpath /z/--help ./--help</font></pre>
    </div>
</div>
</div>
<div ID="freeplaneNode__ID_1308908269" class=FreeplaneNodeGen>
    <p>
      <font size="1">[2] the file-named ./--version will be translated by wildcard-expansion as &quot;--version&quot;&#160;&#160;&#160;which the command will interpreted mistakenly as an option-argument &quot;--version&quot; and&#160;&#160;so will mislead the execution and intended-behaviour of the command &quot;ls&quot;</font>
    </p>
</div>
<div ID="freeplaneNode__ID_1689947434" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      Did it shock you too? I almost could not believe when I read it.
    </p>
    <p>
<br>
    </p>
    <p>
      Basically the wildcard expansion can (kind-of) inject unexpected options 
      into a command, by using carefully crafted filenames to interfere with 
      command execution
    </p>
    <p>
<br>
    </p>
    <p>
      In any case, its not completely visible the reach of this at first sight 
      - think about we did there, that little confusion we caused in &quot;ls&quot; and 
      &quot;rm&quot;.
    </p>
    <p>
      This confusion happens because we have a <u>shell </u>(bash) doing its 
      normal wildcard-expansion, that will transform the wildcard '*' into a 
      list of arguments, where in turn will exist the 
      specially-crafted-filename, that will be passed in to the command as one 
      more string argument, and the command that received all string arguments 
      is misguided to confuse it with an authentic --option string.
    </p>
    <p>
      And this is very prevalent with non-hardened-commands. It happens to my 
      programs, my colleagues scripts, official linux binaries, common root 
      tools... oh my, you already see how vast this reaches, right?
    </p>
    <p>
<br>
    </p>
    <p>
      The root-analysis of this problem goes back to the source-code of each 
      affected program.
    </p>
    <p>
      Typically when a <u>program </u>is run in a shell, the shell will send the 
      arguments to the program as strings. Imagine now that the program has 
      this &quot;--optionX&quot; builtin and when it receives the arguments it will try 
      to detect if any of them is &quot;--optionX&quot; . If it finds it, it thinks that 
      its an option, as has no immediate reason to check if that was also the 
      name of the file of its current directory. So by crafting 
      special-filenames and placing them in the correct directory, it is 
      possible to attack commands that use wildcard-expansion, to misguide its 
      execution. With such strategies, its plausible to leverage privilege 
      escalation and so complete compromise of a system. And its not so 
      uncommon - Imagine misguiding the execution of a root script, that 
      contains a <u><b>tar xxx xxx *</b></u>&#160;to be executed in /tmp/ - it would be 
      possible to strategically use some <u><i>tar options</i></u>&#160;&#160;and get a 
      compromised root access.
    </p>
</div>
<div ID="freeplaneNode__ID_1628466868" class=FreeplaneNodeGen>
    <p>
      Note that you can <u>never</u>&#160;put a '/' as a character in a filename, because at the linux kernel layer, the system-calls that intervene in the filesystem-path-transversal-and-filename-detection&#160;&#160;will <u>never</u>&#160; allow the '/' character as part of any filename.
    </p>
    <p>
      This restriction from the kernel, applies to everything kernel-down: libc library-calls, to all the compiled dynamic-libs, all binaries and shells, in all filesystem-types. It applies to all linux, and its impossible to change.
    </p>
    <p>
      So never (try to) put a '/' as a character in a filename, its impossible (or a new file-system bug which will&#160;&#160;render the system to a fault in very short time)
    </p>
    <p>
      Have a look at the <a href="https://stackoverflow.com/a/9847573/259192">details so well explained in this stackoverflow thread</a><br><br>
    </p>
    <p>
      Also, note that putting a space &quot; &quot; in a filename is possible, but the wildcard-expansion will treat it as any character and consider the filename as one single argument which contains a space inside. So, just to be clear - it will never split the filename into multiple words (space-separated) and pass each word as a separate argument. No, it will always pass the entire filename (space included) as one single argument (never multiple)
    </p>
    <p>
<br>
    </p>
    <p>
      I've made this short demo for myself while trying to understand
    </p>
</div>
<div ID="freeplaneNode__ID_1202675977" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <div bgcolor="#333333" color="#808080">
      <pre face="Courier New" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal"><span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">show_args()&lbrace; echo &quot;1__$&lbrace;1&rbrace;__&quot;; echo &quot;2__$&lbrace;2&rbrace;__&quot;; echo &quot;3__$&lbrace;3&rbrace;__&quot;;&rbrace;</font></span>
<span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">show_args aaa bbb ccc</font></span>
1__aaa__
2__bbb__
3__ccc__
<span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">show_args aaa bbb</font></span>
1__aaa__
2__bbb__
3____
<span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">show_args &quot;aaa bbb&quot;</font></span>
1__aaa bbb__
2____
3____
<span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">show_args &quot;aaa b b b &quot;</font></span>
1__aaa b b b __
2____
3____
<span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">ll</font></span>
total 8
drwxrwxr-x  2 ub   ub   4096 Apr 27 00:48  ./
drwxrwxrwt 13 root root 4096 Apr 27 00:48  ../
-rw-rw-r--  1 ub   ub      0 Apr 26 23:44  a
-rw-rw-r--  1 ub   ub      0 Apr 26 23:44  b
-rw-rw-r--  1 ub   ub      0 Apr 27 00:16 '--full-time tmp2'
<span face="Courier New" bgcolor="#4c4c4c" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@seclab:/tmp/d$ <font color="#ffffff">show_args *</font></span>
1__a__
2__b__
3__--full-time tmp2__
ub@seclab:/tmp/d$</pre>
    </div>
</div>
</div>
<div ID="freeplaneNode__ID_1892364384" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
      In any case, this comes with restrictions - for example, you need to 
      name the files specifically for the individual commands to attack. Or 
      the files need to be placed in specific locations for the filenames to 
      appear without any undesirable /prefix/path.
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      So, what can be done? Invest time/budget to educate programmers (me 
      included) and script-creators with security-focus best-practices, to 
      avoid this and other similar situations.
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      Hope this has been interesting to you too :) You can leave a comment 
      someday when I have time to integrate this blog with Disqus :)&#160;
    </p>
    <p>
      <br>
      <br>
<br>
    </p>
    <p>
      Thankgiving and &quot;props where props are due&quot;, to:
    </p>
    <p>
      &#160;&#160;Exploiting Wildcard for Privilege Escalation
    </p>
    <p>
      <a href="https://www.hackingarticles.in/exploiting-wildcard-for-privilege-escalation/">&#160; 
      https://www.hackingarticles.in/exploiting-wildcard-for-privilege-escalation/</a>
    </p>
</div>
<div ID="freeplaneNode__ID_1109830603" class=FreeplaneNodeGen>
    <p>
      NOTE: make you think about the -e option of grep:&#160; <b>grep -e 
      '-regexp starting with-'</b>
    </p>
</div>
</div>
