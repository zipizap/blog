---
title: "templuxinator"
date: 2021-08-11


draft: false

categories:
#- Infosec
- Devops
- Sysadmin
- Cloud
#- K8s
- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
#- cve
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
#- bash
#- ansible
#- quickref
#- k8s
#- k3s
#- k3d
- github
- helm
- templating
---
<div>
<div ID="freeplaneNode__ID_1874570387" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">templuxinator</font></b><br><br><a href="https://github.com/zipizap/templuxinator">https://github.com/zipizap/templuxinator </a><br><br>Think 
      helm-charts-templates, but for any file type :) without helmet :)<br><br><br><br><b><font size="4">This 
      is... what?</font></b><br><br>I didnt like helm-chart templating.
    </p>
    <p>
      Then I learned it.
    </p>
    <p>
      And wanted it for any file, without helm
    </p>
    <p>
<br>
    </p>
    <p>
      templuxinator - simple templating engine to bake <b>mytemplatefile.anything</b>&nbsp; 
      + <b>myvaluesfile.yaml</b>&nbsp;= <b>theresultfile.anything</b>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;+ <b>mytemplatefile.anything </b>should be a text file (UTF-8) 
      in any format (.js, .yaml, .toml, anything), containing text and 
      template-expressions (like &lbrace;&lbrace; .person2.name &rbrace;&rbrace; )<br>&nbsp;&nbsp;&nbsp;&nbsp;The template can 
      have <a href="https://golang.org/pkg/text/template/#hdr-Functions">golang template functions</a>&nbsp;+ and <a href="http://masterminds.github.io/sprig/">sprig functions</a>&nbsp; 
      (very much like helm charts - almost the SAME indeed! )
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;+ <b>myvaluesfile.yaml </b>should be a valid yaml file 
      (UTF-8), the values that can be used inside the template
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;+ <b>theresultfile.anything </b>will be created, containing a 
      copy of the template with the template-expressions parsed
    </p>
    <p>
<br>
    </p>
    <p>
      No more, no less, simple.<br><br>Demo in Asciinema in <a href="https://asciinema.org/a/413908?autoplay=1">this link</a>&nbsp;
    </p>
</div>
<div ID="freeplaneNode__ID_1695626011" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_7081481227992992262.png"
    link="png_7081481227992992262.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_398884553" class=FreeplaneNodeGen>
<p>This was barely tested, but the code is so simple that there is not much to fail :)</p>
<p>Golang makes it all very straightforward, I just connected the pieces: golang template/sprig lib, cli command arguments, and some file-read/writing :)</p>
<p></p>
<p>Open-source 4 life ;)</p></div>
</div>
