---
title: "LXD 4 docker inside lxc-container (2layer virtualization)"
date: 2019-12-31 


draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- 4hackers
- 4nonhackers
- informative
- lxd
- lxc
- docker
#- exploitation
#- suid
#- etc
#- portugues
#- sociology

---
<div>
<div ID="freeplaneNode__ID_1630286972" class=FreeplaneNodeGen>
    <p>
      In a lxc-host, we launch a <b>lxc-container </b>myUb1804, and then <u>inside the lxc-container we install docker and launch a <b>docker container</b></u>. So, this explanation focus towards <u>how to run docker, inside a lxc-container</u>.&#160;&#160;:)
    </p>
</div>
<div ID="freeplaneNode__ID_1536667585" class=FreeplaneNodeGen>
<p></p></div>
<div ID="freeplaneNode__ID_1576057283" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Resume of the procedure</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_292433550" class=FreeplaneNodeGen>
    <p>
      Run the followin, in <b>lxc-host</b>, as a user with access to lxc/lxd
    </p>
</div>
<div ID="freeplaneNode__ID_528623191" class=FreeplaneNodeGen>
<p>We first create the lxc-container</p></div>
<div ID="freeplaneNode__ID_1467116480" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      MyContainer=<font color="#ffffff">myUb1804</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1199792328" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc launch ubuntu-daily:18.04 $MyContainer</p></div>
</div>
<div ID="freeplaneNode__ID_1330187117" class=FreeplaneNodeGen>
<p>And we set these special properties, necessary for docker to execute properly inside</p></div>
<div ID="freeplaneNode__ID_767984138" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc config set $MyContainer security.nesting true</p>
<p>lxc config get $MyContainer security.nesting</p></div>
</div>
<div ID="freeplaneNode__ID_395025077" class=FreeplaneNodeGen>
<p>And we move into a shell inside the lxc-container</p></div>
<div ID="freeplaneNode__ID_1748349104" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc exec $MyContainer -- bash</p></div>
</div>
<div ID="freeplaneNode__ID_682870772" class=FreeplaneNodeGen>
    <p>
      At this point, we are left in the <b>lxc-container</b>, as <b>root</b>
    </p>
</div>
<div ID="freeplaneNode__ID_612305179" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b>Install Docker </b>
    </p>
</div>
<div ID="freeplaneNode__ID_663683758" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>apt -y update &amp;&amp; apt -y upgrade</p></div>
</div>
<div ID="freeplaneNode__ID_1624251384" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>apt install -y docker.io</p></div>
</div>
<div ID="freeplaneNode__ID_1007424781" class=FreeplaneNodeGen>
    <p>
      <font size="1">NOTE: docker should not be installed from snap, like &quot;snap install docker&quot; as at least for me it did not work correctly inside lxc-container... snap adds some special restrictions which I believe conflict with this lxc-container/docker scenario... So, install docker.io&#160;&#160;via apt</font>
    </p>
</div>
<div ID="freeplaneNode__ID_1424886310" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b>Check docker storage driver </b>
    </p>
</div>
<div ID="freeplaneNode__ID_1417231883" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>docker info</p>
<p>&nbsp;&nbsp;# verify Storage Driver: **vfs**</p></div>
</div>
<div ID="freeplaneNode__ID_1962085225" class=FreeplaneNodeGen>
<p>Example</p></div>
<div ID="freeplaneNode__ID_1516275948" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#ffffff">root@ub1804T1:~# docker info</font>
    </p>
    <p>
      <font color="#999999">... </font>
    </p>
    <p>
      <font color="#999999">Server Version: 18.09.5 </font>
    </p>
    <p>
      <font color="#ffff00">Storage Driver: vfs</font>
    </p>
    <p>
      <font color="#999999">Logging Driver: json-file </font>
    </p>
    <p>
      <font color="#999999">...</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_88669031" class=FreeplaneNodeGen>
    <p>
      The <u><i>vfs driver</i></u>&#160;is ok. If you want to go into something fancier, think well about its implications on lxc-container layer... especially if you need additional kernel-modules loaded for the docker-storage driver, which have to be loaded in the lxc-host and then &quot;authorized&quot; into the lxc-container via lxc options... yeah, at your own risk :)&#160;&#160;In short, vsf is the simplest option, and enough for this demo
    </p>
    <p>
      For more references, see
    </p>
    <p>
      <a href="https://docs.docker.com/storage/storagedriver/select-storage-driver/">https://docs.docker.com/storage/storagedriver/select-storage-driver/</a>&#160;)
    </p>
</div>
<div ID="freeplaneNode__ID_1355195257" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b>Launch a docker container</b>
    </p>
    <p>
<br>
    </p>
    <p>
      At this point we have docker installed inside the lxc-container.
    </p>
    <p>
      Lets now launch a couple of docker containers, to verify they download-and-run without any problem.
    </p>
</div>
<div ID="freeplaneNode__ID_1536798745" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>docker run hello-world</p></div>
</div>
<div ID="freeplaneNode__ID_764753887" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>docker run -it --rm ubuntu bash</p></div>
</div>
<div ID="freeplaneNode__ID_1722211829" class=FreeplaneNodeGen>
    <p>
      And it's done - we have a <u><i>docker container</i></u>, running inside a <u><i>lxc-container</i></u>, running inside a lxc-host: one same kernel providing 2 nested layers of container virtualization.
    </p>
    <p>
<br>
    </p>
    <p>
      Where this might be a good fit?
    </p>
    <p>
      Well, LXC/LXD has some advantages while DOCKER has other different advantages. They do not compete for the same usecase, <font color="#afbac4"><b>they are complementary</b></font>.<br>There are things you want to do in a VM (lxc-container) that are unfit to be done inside a docker-container... for example, adding additional network firewall/interfaces for multiple vlans/overlaysubnets/vpn...
    </p>
    <p>
<br>
    </p>
    <p>
      And other things you should do at the DOCKER level that shoul be transparent to the VM (lxc-container), like changes/updates of docker apps and its deployment-tactics
    </p>
</div>
<div ID="freeplaneNode__ID_1595566639" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">Detailed example from start-to-finish </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_638239516" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <div bgcolor="#333333" color="#808080">
      <pre face="Courier New" style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; font-weight: normal; font-style: normal">ub@lxchosta:~$ <font color="#ffffff">lxc list</font>
+--------------+---------+------+------+------------+-----------+
|     NAME     |  STATE  | IPV4 | IPV6 |    TYPE    | SNAPSHOTS |
+--------------+---------+------+------+------------+-----------+
| myContainer3 | STOPPED |      |      | PERSISTENT | 0         |
+--------------+---------+------+------+------------+-----------+
ub@lxchosta:~$ <font color="#ffffff">lxc launch images:ubuntu/18.04/amd64   myUb1804</font>
Creating myUb1804
Starting myUb1804
ub@lxchosta:~$ <font color="#ffffff">lxc list</font>
+--------------+---------+---------------------+----------------------------------------------+------------+-----------+
|     NAME     |  STATE  |        IPV4         |                     IPV6                     |    TYPE    | SNAPSHOTS |
+--------------+---------+---------------------+----------------------------------------------+------------+-----------+
| myContainer3 | STOPPED |                     |                                              | PERSISTENT | 0         |
+--------------+---------+---------------------+----------------------------------------------+------------+-----------+
| <font color="#9999ff">myUb1804</font>     | RUNNING | 10.37.204.12 (eth0) | fd42:fac:3bb4:3213:216:3eff:fea7:88c5 (eth0) | PERSISTENT | 0         |
+--------------+---------+---------------------+----------------------------------------------+------------+-----------+
ub@lxchosta:~$ <font color="#ffffff">MyContainer=</font><font color="#9999ff">myUb1804</font>
ub@lxchosta:~$
ub@lxchosta:~$ <font color="#ffff33">lxc config set $MyContainer security.nesting true</font>
ub@lxchosta:~$ <font color="#ffff33">lxc config get $MyContainer security.nesting</font>
true
ub@lxchosta:~$ <font color="#ffffff">lxc exec $MyContainer -- bash</font>
root@myUb1804:~# <font color="#ffffff">apt -y update &amp;&amp; apt -y upgrade</font>
Hit:1 http://archive.ubuntu.com/ubuntu bionic InRelease
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
Get:3 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Fetched 177 kB in 1s (314 kB/s)
Reading package lists... Done
Building dependency tree
Reading state information... Done
All packages are up to date.
Reading package lists... Done
Building dependency tree
Reading state information... Done
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
root@myUb1804:~# <font color="#ffffff">apt install -y docker.io</font>
...long boring printout cut for brevety...
root@myUb1804:~#
root@myUb1804:~# <font color="#ffffff">docker info</font>
Containers: 0
&#160;Running: 0
&#160;Paused: 0
&#160;Stopped: 0
Images: 0
Server Version: 18.09.7
<font color="#9999ff">Storage Driver: vfs</font>
Logging Driver: json-file
Cgroup Driver: cgroupfs
Plugins:
&#160;Volume: local
&#160;Network: bridge host macvlan null overlay
&#160;Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
Swarm: inactive
Runtimes: runc
Default Runtime: runc
Init Binary: docker-init
containerd version:
runc version: N/A
init version: v0.18.0 (expected: fec3683b971d9c3ef73f284f176672c44b448662)
Security Options:
&#160;apparmor
&#160;seccomp
&#160;&#160;Profile: default
Kernel Version: 4.15.0-72-generic
Operating System: Ubuntu 18.04.3 LTS
OSType: linux
Architecture: x86_64
CPUs: 1
Total Memory: 1.947GiB
Name: myUb1804
ID: HVC7:J35B:4C24:BVVJ:UASE:67SA:VBUA:XX65:HTJJ:5JQI:G3SE:FUTZ
Docker Root Dir: /var/lib/docker
Debug Mode (client): false
Debug Mode (server): false
Registry: https://index.docker.io/v1/
Labels:
Experimental: false
Insecure Registries:
&#160;127.0.0.0/8
Live Restore Enabled: false

WARNING: No swap limit support
WARNING: bridge-nf-call-iptables is disabled
WARNING: bridge-nf-call-ip6tables is disabled
root@myUb1804:~#
root@myUb1804:~#
root@myUb1804:~# <font color="#ffffff">docker run hello-world</font>
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: sha256:4fe721ccc2e8dc7362278a29dc660d833570ec2682f4e4194f4ee23e415e1064
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the &quot;hello-world&quot; image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

root@myUb1804:~# <font color="#ffffff">docker run -it --rm ubuntu bash</font>
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
2746a4a261c9: Pull complete
4c1d20cdee96: Pull complete
0d3160e1d0de: Pull complete
c8e37668deea: Pull complete
Digest: sha256:250cc6f3f3ffc5cdaa9d8f4946ac79821aafb4d3afc93928f0de9336eba21aa4
Status: Downloaded newer image for ubuntu:latest
root@5921603e7a0c:/#
root@5921603e7a0c:/#
root@5921603e7a0c:/# <font color="#ffffff">cat /etc/os-release</font>
NAME=&quot;Ubuntu&quot;
VERSION=&quot;18.04.3 LTS (Bionic Beaver)&quot;
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME=&quot;Ubuntu 18.04.3 LTS&quot;
VERSION_ID=&quot;18.04&quot;
HOME_URL=&quot;https://www.ubuntu.com/&quot;
SUPPORT_URL=&quot;https://help.ubuntu.com/&quot;
BUG_REPORT_URL=&quot;https://bugs.launchpad.net/ubuntu/&quot;
PRIVACY_POLICY_URL=&quot;https://www.ubuntu.com/legal/terms-and-policies/privacy-policy&quot;
VERSION_CODENAME=bionic
UBUNTU_CODENAME=bionic
root@5921603e7a0c:/# <font color="#ffffff">exit</font>
exit
root@myUb1804:~#</pre>
    </div>
</div>
</div>
<div ID="freeplaneNode__ID_1685469624" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">References</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_838150529" class=FreeplaneNodeGen>
<p>https://discuss.linuxcontainers.org/t/starting-docker-inside-lxd-18-04/4393</p></div>
<div ID="freeplaneNode__ID_645892935" class=FreeplaneNodeGen>
    <p>
      <a href="https://lxd.readthedocs.io/en/latest/#how-can-i-run-docker-inside-a-lxd-container">https://lxd.readthedocs.io/en/latest/#how-can-i-run-docker-inside-a-lxd-container</a>
    </p>
</div>
</div>
