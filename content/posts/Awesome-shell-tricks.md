---
title: "Awesome shell tricks"
date: 2020-12-03


draft: false

categories:
- Infosec
- Devops
- Sysadmin
#- Cloud
#- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
- bash
#- ansible
- quickref
- k8s
---
<div>
<div ID="freeplaneNode__ID_352385396" class=FreeplaneNodeGen>
    <p>
      <b><font size="4">Show OPEN ports, aka rudimentary ports-scanning :)</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_1454847155" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>for p in &lbrace;1..1024&rbrace;; do (:&gt;/dev/tcp/127.0.0.1/$p) &amp;&gt;/dev/null &amp;&amp; echo "OPEN $p"; done</p></div>
</div>
<div ID="freeplaneNode__ID_604922978" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_429842813705833051.png"
    link="png_429842813705833051.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1248562854" class=FreeplaneNodeGen>
<p>.</p></div>
<div ID="freeplaneNode__ID_1072154614" class=FreeplaneNodeGen>
<p>.</p></div>
<div ID="freeplaneNode__ID_52652116" class=FreeplaneNodeGen>
<p>.</p></div>
<div ID="freeplaneNode__ID_386929287" class=FreeplaneNodeGen>
    <p>
      <b><font size="4">Frenzy test dns-resolution :)</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_277063204" class=FreeplaneNodeGen>
<p>Remember the mythical fork-bomb ?</p></div>
<div ID="freeplaneNode__ID_1108733877" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>:()&lbrace; :|:&amp; &rbrace;;:</p></div>
</div>
<div ID="freeplaneNode__ID_1296939148" class=FreeplaneNodeGen>
<p>.</p></div>
<div ID="freeplaneNode__ID_924161117" class=FreeplaneNodeGen>
<p>With that ugly-but-slick syntax in mind, we can test a command exit-code with a nice pretty-print of "." or "E" for non-zero exit-codes :)</p></div>
<div ID="freeplaneNode__ID_1249106080" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>&nbsp;:()&lbrace; getent hosts www.google.com &amp;&gt;/dev/null &amp;&amp; echo -n '.' || echo -n 'E';:;&rbrace;;:</p></div>
</div>
<div ID="freeplaneNode__ID_1199849943" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>:()&lbrace; (echo $RANDOM | grep 7) &amp;&gt;/dev/null &amp;&amp; echo -n '.' || echo -n 'E';:;&rbrace;;:</p></div>
</div>
<div ID="freeplaneNode__ID_1846658089" class=FreeplaneNodeGen>
<p>.</p>
{{< figure
    src="png_1009319175901838273.png"
    link="png_1009319175901838273.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_429260521" class=FreeplaneNodeGen>
<p></p></div>
<div ID="freeplaneNode__ID_1110492739" class=FreeplaneNodeGen>
<p>And we can do it with a more familiar syntax (to don't scare anyone off)</p></div>
<div ID="freeplaneNode__ID_1345974943" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      while :; do (<font color="#ffffff">echo $RANDOM | grep 7</font>) &amp;&gt;/dev/null &amp;&amp; echo -n '.' || 
      echo -n 'E'; done
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_972541810" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_488907229613975100.png"
    link="png_488907229613975100.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_729731450" class=FreeplaneNodeGen>
<p>Really usefull :)</p></div>
<div ID="freeplaneNode__ID_592437255" class=FreeplaneNodeGen>
<p>.</p></div>
</div>
