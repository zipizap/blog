---
title: "Inauguration"
date: 2019-03-27


draft: false

categories: 
#- Infosec
#- Devops
#- Sysadmin
#- Cloud
#- Programming

tags:
- informative
#- exploitation
#- suid
#- etc

---
<div>
<div ID="freeplaneNode__ID_1558018065" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_6683585350652446059.png"
    link="png_6683585350652446059.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1698993532" class=FreeplaneNodeGen>
    <p>
      Its finally done!
    </p>
    <p>
<br>
    </p>
    <p>
      I've created my blog, for infosec/cloud/devops - its a strange combination that keeps magnetizing me more-and-more.<br>To be honest, I feel fortunate to live in this epoch, when I can see some of my passions going&#160;&#160;through a &quot;sysadmin renaissance&quot; takeoff...<br><br>I remeber listening to an interview where the docker-founders were explaining how difficult it was for them to go and evangelise the sys/ops and devs communities, to introduce this new idea of containers automation&#160;&#160;- they put in serious efforth not only with sys-programming and kernel-namespaces but also in promoting to a critical mass of interest/adoption, which eventually growed exponentially...<br><br>Also feels like from another lifetime (and was no more than a decade, but how fast it exploded), when cybersec grew from underground-movement into a fullblown public safety and security requirement... there are now books, masters, conferences, open talent competitions, zillion youtube channels (too much low-quality tbh)... and the media seems to have a party with drama, stereotypes and wiz-kids. Yes, the media - the media curbed its own change of heart (for good this time), and fortunately there is now much better awareness of security to confront the current-day threats.
    </p>
    <p>
<br>
    </p>
    <p>
      Internet also happens to be a much more pervasive need in everyone's lives - laptops, mobiles, tablets, clocks, cars, IoT, IaaS, PaaS, SaaS, etc...
    </p>
    <p>
      Live communications and internet is the new pulse of society, and it embodies a very profound meaning: its an invisible baseline for us, an infra-requisite of modern society, the new vehicle for interpersonal communications, the invisible substrate of interconnection in a world where everything is more and more closing together... Internet has become universal, and will keep going forward and stronger. Its not the interneta anymore, its&#160;everyones lifestyle.
    </p>
    <p>
<br>
    </p>
    <p>
      So, security in this universe of information, where millions of people breadth information daily, is paramount, critical. And here is where cybersec has become meaningfull to me - it helps improve people's lifes, in a very realistic/palpable sense - and to me this drives and emanates motivation to all cells of my body :) It resonates deeply :) I have to calm-down from all the excitement and fascination it produces me :)
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      Now that its explained a bit about the underlying personal motivations, lets let some geeky tech-details come out :)
    </p>
    <p>
<br>
    </p>
    <p>
      This blog was purposedly made as a static-website, is updated/deployed with CI/CD via gitlab using docker containers, and is rendered with hugo - a multi-platform single-static-binary.<br>On top of it,&#160;&#160;all the interactions to create new content and posts, were automated and simplified into a freeplane mindmap, where the content is prepared, and deployed via a tailor-made jruby program
    </p>
    <p>
<br>
    </p>
    <p>
      The overall effect,&#160;is to write something at any moment in the mindmap (that I have open everyday, as I'm already a freeplane-poweruser) and get it published in seconds. This enables posting with minimal-use-flow and concentrate more on content. This blog-setup itself makes for a nice story to be told in another post of its own, someday.
    </p>
    <p>
      &#160;
    </p>
    <p>
      <br>
      PS: The antique <a href="http://zipizap.wordpress.com">http://zipizap.wordpress.com</a>&#160;is still online for casual queries, and for the moment will not blend here so soon. That was another time
    </p>
</div>
</div>
