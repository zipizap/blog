---
title: "Phineas Fisher Caiman-papers"
date: 2019-11-19 


draft: false

categories: 
- Infosec
#- Devops
#- Sysadmin
#- Cloud
#- Programming

tags:
- 4hackers
- 4nonhackers
- informative
#- exploitation
#- suid
#- etc
- portugues
- sociology

---
<div>
<div ID="freeplaneNode__ID_1281333205" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_3147429249364375567.png"
    link="png_3147429249364375567.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1926088744" class=FreeplaneNodeGen>
    <p>
      Esta noticia &#233; interesante pela parte sociol&#243;gica, e provavelmente nao ser&#225; retratada nos media da mesma maneira ;)
    </p>
    <p>
<br>
    </p>
    <p>
      Hackearam o banco-para&#237;so-fiscal das Ilhas Caiman (Cayman National Bank and Trust) [1]
    </p>
    <p>
<br>
    </p>
    <p>
      O &quot;hacker&quot; publicou um manifesto, onde diz que roubou ?centenas de milhoes? que repartiu entre organiza&#231;oes sociais (estilo robin hood) [2] (leiam o manifesto s&#243; no fim, &#233; o mais interessante para aprofundar)
    </p>
    <p>
<br>
    </p>
    <p>
      Tambem esperou pelo data de aniversario do Tupac Katari [3] (heroi da Bolivia da rebeliao contra colonos espanhois) para publicar na internet a informa&#231;ao interna do banco (1TB) .
    </p>
    <p>
<br>
    </p>
    <p>
      Essa informa&#231;ao vai certamente ser escrutinizada e investigada publicamente - que provavelmente destapar&#225; mais casos de corrup&#231;ao e opera&#231;oes escondidas de dinheiro-negro. As Ilhas Caiman pelos vistos sao consideradas um dos mais famosos paraisos-fiscais [5].
    </p>
    <p>
      Ou seja, isto &#233; uma esp&#233;cie de Panama-papers das Ilhas Caiman.
    </p>
    <p>
<br>
    </p>
    <p>
      O &quot;hacker&quot; que reinvindica o ataque &#233; famoso - Phineas Fisher - por ter motivos anarquistas/filantropicos e publicar manifestos [2] com motiva&#231;oes contra-poder e desigualdades-sociais.<br>
    </p>
    <p>
<br>
    </p>
    <p>
      E esta &#233; a razao de escrever tudo isto - o manifesto do hacker [2], tem pontos interessantes :)
    </p>
    <p>
      Lembram-se daquele sentimento social que falamos de &quot;crescente insatisfa&#231;ao social e desigualdade?&quot; - o manifesto faz ressonancia e excita esses n&#243;s delicados :) O resto da novela est&#225; no manifesto se quizerem ler.
    </p>
    <p>
<br>
    </p>
    <p>
      A&#237; fica para discussao :) esse wassap a arder :)
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      PS2: Se alguem lhe parece interessante o personagem este do Phineas Fisher (?como nao, at&#233; tem sentido de humor :) ), vejam o relato deste outro ataque onde ele hackeou um grupo-de-hackers-mercenarios italianos que vendiam servi&#231;os de espionagem a governos, para revelar que 70% era espiar ONGs, jornalistas, adversarios politicos, e 30% espiar o crime (mas tudo pago com dinheiro publico :)) E vejam que na lista aparecem mais &quot;paises democraticos&quot; do que o que ser&#237;a de esperar... :) Uma entrevista exclusiva do Phineas Fisher com um boneco dos marretas[6c] (curto e divertido), a cobertura-magazine do hack [6a] (mais extenso), o seu manifesto [6b] (apenas introdu&#231;ao), e a lista de &quot;paises clientes&quot; dos servi&#231;os de espionagem [6d]. (espanha est&#225; l&#225;)
    </p>
    <p>
      &#160;
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      &#160;
    </p>
    <p>
      [1] <a href="https://twitter.com/DDoSecrets/status/1195899716653010945">https://twitter.com/DDoSecrets/status/1195899716653010945</a>
    </p>
    <p>
      [2] <a href="https://archive.is/ldGvQ#selection-307.2170-307.2180">https://archive.is/ldGvQ#selection-307.2170-307.2180</a>
    </p>
    <p>
      [3] <a href="https://pt.wikipedia.org/wiki/T%C3%BApac_Katari">https://pt.wikipedia.org/wiki/T%C3%BApac_Katari</a>
    </p>
    <p>
      [4] <a href="https://archive.is/ldGvQ#selection-307.2170-307.2180">https://archive.is/ldGvQ#selection-307.2170-307.2180</a>
    </p>
    <p>
      [5] <a href="https://www.investopedia.com/ask/answers/100215/why-cayman-islands-considered-tax-haven.asp">https://www.investopedia.com/ask/answers/100215/why-cayman-islands-considered-tax-haven.asp</a>&#160; (esta fonte &#233; bastante foleirinha, mas d&#225; para ter uma ideia :) )
    </p>
    <p>
      [6a] <a href="https://crimethinc.com/2018/06/05/hackback-talking-with-phineas-fisher-hacking-as-direct-action-against-the-surveillance-state">https://crimethinc.com/2018/06/05/hackback-talking-with-phineas-fisher-hacking-as-direct-action-against-the-surveillance-state</a>
    </p>
    <p>
      [6b] <a href="http://pastebin.com/raw/0SNSvyjJ">http://pastebin.com/raw/0SNSvyjJ</a>
    </p>
    <p>
      [6c] <a href="https://www.youtube.com/watch?v=BpyCl1Qm6Xs">https://www.youtube.com/watch?v=BpyCl1Qm6Xs</a>
    </p>
    <p>
      [6d] <a href="https://en.wikipedia.org/wiki/Hacking_Team#Customer_list">https://en.wikipedia.org/wiki/Hacking_Team#Customer_list</a>
    </p>
</div>
</div>
