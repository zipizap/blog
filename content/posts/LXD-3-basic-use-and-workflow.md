---
title: "LXD 3 basic use and workflow"
date: 2019-12-28 


draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- 4hackers
- 4nonhackers
- informative
- lxd
- lxc
#- exploitation
#- suid
#- etc
#- portugues
#- sociology

---
<div>
<div ID="freeplaneNode__ID_1626397649" class=FreeplaneNodeGen>
    <p>
      The last post &quot;<a href="posts/lxd-install-via-snap/">LXD install via snap</a>&quot; was a bit extense... so lets break off and just &quot;take the bike for a ride&quot; to feel why LXD can be fun.
    </p>
    <p>
<br>
    </p>
    <p>
      Without any further addo, lets see a basic workflow for LXD :)
    </p>
    <p>
      It assumes LXD is already installed and ready to be used
    </p>
</div>
<div ID="freeplaneNode__ID_1542765255" class=FreeplaneNodeGen>
    <p>
      <br>
      <br>
      <b><font size="4">Launch a new container </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_550278552" class=FreeplaneNodeGen>
<p>Choose one image</p></div>
<div ID="freeplaneNode__ID_1790306455" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc image list images:</p></div>
</div>
<div ID="freeplaneNode__ID_719219213" class=FreeplaneNodeGen>
    <p>
      Download image and create <b>myContainer </b>from it
    </p>
</div>
<div ID="freeplaneNode__ID_1949997650" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      ## Some usefull examples :)
    </p>
    <p>
      #lxc launch images:centos/7/amd64&#160;&#160;&#160;&#160;&#160;&#160;&#160;myCent
    </p>
    <p>
      #lxc launch images:ubuntu/18.04/amd64&#160;&#160;&#160;myUb
    </p>
    <p>
      #lxc launch images:alpine/3.10/amd64&#160;&#160;&#160;&#160;myAlp
    </p>
    <p>
      #lxc launch images:kali&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;myKali
    </p>
    <p>
      #lxc launch images:debian/stretch&#160;&#160;&#160;&#160;&#160;&#160;&#160;myDeb9
    </p>
    <p>
<br>
    </p>
    <p>
      lxc launch images:debian/stretch&#160;&#160;&#160;&#160;&#160;&#160;&#160;<font color="#ff99ff">myContainer</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_993920131" class=FreeplaneNodeGen>
    <p>
      At this point, <b>myContainer </b>should be up and running
    </p>
</div>
<div ID="freeplaneNode__ID_1177674434" class=FreeplaneNodeGen>
    <p>
      <br>
<br>
    </p>
    <p>
      <b><font size="4">Manage existing containers</font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_532772133" class=FreeplaneNodeGen>
<p>List existing containers</p></div>
<div ID="freeplaneNode__ID_1696419420" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>lxc list</p></div>
</div>
<div ID="freeplaneNode__ID_1516535656" class=FreeplaneNodeGen>
    <p>
      <br>
    </p>
</div>
<div ID="freeplaneNode__ID_948639945" class=FreeplaneNodeGen>
    <p>
      A <u><i>stopped</i></u>&#160;container can be <u><i>start</i></u><i>&#160;</i>'ed:
    </p>
</div>
<div ID="freeplaneNode__ID_655499496" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      # myContainer is &quot;STOPPED&quot;
    </p>
    <p>
<br>
    </p>
    <p>
      lxc start <font color="#ff99ff">myContainer</font>
    </p>
    <p>
<br>
    </p>
    <p>
      # myContainer is now &quot;RUNNING&quot;
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1962687141" class=FreeplaneNodeGen>
    <p>
      <br>
    </p>
</div>
<div ID="freeplaneNode__ID_1505173466" class=FreeplaneNodeGen>
    <p>
      We can <u><i>execute</i></u>&#160;a shell in a running container.
    </p>
    <p>
      <font size="1">In fact, we can execute any tty-interactive command, kind-of-like a ssh-session :) ) </font>
    </p>
</div>
<div ID="freeplaneNode__ID_1499324562" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      lxc exec <font color="#ff99ff">myContainer </font>-- <font color="#ccccff">bash</font><br><br>## NOTE: if bash shell is not installed - as it happens with alpine-linux, instead try sh shell<br>## lxc exec <font color="#ff99ff">myContainer</font>&#160;-- <font color="#ccccff">sh</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_616506540" class=FreeplaneNodeGen>
<p>The "--" separates the left-side lxd-arguments, from the right-side where the container-command start</p></div>
<div ID="freeplaneNode__ID_883622270" class=FreeplaneNodeGen>
    <p>
      <br>
    </p>
</div>
<div ID="freeplaneNode__ID_865624125" class=FreeplaneNodeGen>
    <p>
      We can also <u><i>copy</i></u>&#160;files lxchost &lt;--&gt; myContainer, and even <u><i>edit</i></u>&#160; it from the lxchost
    </p>
</div>
<div ID="freeplaneNode__ID_1256476977" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      # File copy: host -&gt; myContainer
    </p>
    <p>
      lxc file push hostFile myContainer/etc/path/for/file/<br>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1267840360" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p># File copy: host &lt;- myContainer</p>
<p>lxc file pull myContainer/etc/path/to/file/in/container path/in/host/</p></div>
</div>
<div ID="freeplaneNode__ID_1527798351" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p># Edit file inside myContainer, from the host</p>
<p># The file is pulled from myContainer, edited locally in host, and pushed to myContainer again</p>
<p>lxc file edit myContainer/path/to/container/file/to/edit</p></div>
</div>
<div ID="freeplaneNode__ID_340139310" class=FreeplaneNodeGen>
    <p>
      <br>
    </p>
</div>
<div ID="freeplaneNode__ID_719551410" class=FreeplaneNodeGen>
<p>We can see some basic resource-usage (cpu/ram/network) over a running container</p></div>
<div ID="freeplaneNode__ID_546860201" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      ubuntu@lxchosta:~$ lxc info <font color="#ff99ff">myContainer</font>
    </p>
    <p>
      Name: myContainer
    </p>
    <p>
      Location: none
    </p>
    <p>
      Remote: unix://
    </p>
    <p>
      Architecture: x86_64
    </p>
    <p>
      Created: 2019/05/21 08:55 UTC
    </p>
    <p>
      Status: Running
    </p>
    <p>
      Type: persistent
    </p>
    <p>
      Profiles: default
    </p>
    <p>
      Pid: 1123
    </p>
    <p>
      Ips:
    </p>
    <p>
      &#160;&#160;eth0: inet&#160;&#160;&#160;&#160;10.21.217.181&#160;&#160;&#160;vethda476990
    </p>
    <p>
      &#160;&#160;lo:&#160;&#160;&#160;inet&#160;&#160;&#160;&#160;127.0.0.1
    </p>
    <p>
      Resources:
    </p>
    <p>
      &#160;&#160;Processes: 52
    </p>
    <p>
      &#160;&#160;CPU usage:
    </p>
    <p>
      &#160;&#160;&#160;&#160;CPU usage (in seconds): 12802
    </p>
    <p>
      &#160;&#160;Memory usage:
    </p>
    <p>
      &#160;&#160;&#160;&#160;Memory (current): 276.92MB
    </p>
    <p>
      &#160;&#160;&#160;&#160;Memory (peak): 363.05MB
    </p>
    <p>
      &#160;&#160;Network usage:
    </p>
    <p>
      &#160;&#160;&#160;&#160;eth0:
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Bytes received: 1.50GB
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Bytes sent: 1.35GB
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Packets received: 8148864
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Packets sent: 8066082
    </p>
    <p>
      &#160;&#160;&#160;&#160;lo:
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Bytes received: 9.95MB
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Bytes sent: 9.95MB
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Packets received: 113424
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;Packets sent: 113424
    </p>
    <p>
      Snapshots:
    </p>
    <p>
      &#160;&#160;LAMP (taken at 2019/05/21 09:48 UTC) (stateless)
    </p>
    <p>
      &#160;&#160;beforeDVWAsetupphp (taken at 2019/05/21 10:07 UTC) (stateless)
    </p>
    <p>
      &#160;&#160;DvwaUpAndRunning (taken at 2019/05/22 00:47 UTC) (stateless)
    </p>
    <p>
      &#160;&#160;AllGood (taken at 2019/07/04 09:43 UTC) (stateless)
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_628098558" class=FreeplaneNodeGen>
    <p>
      <br>
    </p>
</div>
<div ID="freeplaneNode__ID_1909495946" class=FreeplaneNodeGen>
    <p>
      We can <i><u>stop</u><b>&#160;</b></i>a running container, either by commanding the container to poweroff itself, or by telling lxd to <i><u>stop --force</u></i>&#160;&#160;the container
    </p>
</div>
<div ID="freeplaneNode__ID_1356367768" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      lxc exec <font color="#ff99ff">myContainer </font>--<font color="#ff99ff">&#160;</font><font color="#ccccff">poweroff</font>
    </p>
    <p>
<br>
    </p>
    <p>
      &#160;&#160;or
    </p>
    <p>
<br>
    </p>
    <p>
      lxc stop --force <font color="#ff99ff">myContainer</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1037272037" class=FreeplaneNodeGen>
    <p>
      <br>
      <b><font size="4">References </font></b>
    </p>
</div>
<div ID="freeplaneNode__ID_823167375" class=FreeplaneNodeGen>
    <ul style="background-color: rgb(255,; background-position: 255, 255); background-image: null; background-repeat: repeat; background-attachment: scroll; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 24px; margin-left: 0; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; list-style: square; color: rgb(51, 51, 51); font-family: Georgia, Bitstream Charter, serif; font-size: 16px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px">
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/03/11/lxd-2-0-introduction-to-lxd-112/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Introduction to LXD [1/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/03/15/lxd-2-0-installing-and-configuring-lxd-212/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Installing and configuring LXD [2/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/03/19/lxd-2-0-your-first-lxd-container-312/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Your first LXD container [3/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/03/26/lxd-2-0-resource-control-412/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Resource control [4/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/03/30/lxd-2-0-image-management-512/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Image management [5/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/04/12/lxd-2-0-remote-hosts-and-container-migration-612/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Remote hosts and container migration [6/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/04/13/lxd-2-0-docker-in-lxd-712/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Docker in LXD [7/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/04/14/lxd-2-0-lxd-in-lxd-812/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: LXD in LXD [8/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/04/25/lxd-2-0-live-migration-912/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Live migration [9/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/06/06/lxd-2-0-lxd-and-juju-1012/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: LXD and Juju [10/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://www.stgraber.org/2016/10/26/lxd-2-0-lxd-and-openstack-1112/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: LXD and OpenStack [11/12]</font></a>
      </li>
      <li align="left" size="0" bgcolor="#ffffff" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; text-align: left; font-size: 0; font-weight: normal; font-style: normal; background-color: #ffffff">
        <a href="https://stgraber.org/2017/02/27/lxd-2-0-debugging-and-contributing-to-lxd-1212/" style="background-image: null; background-repeat: repeat; background-attachment: scroll; background-position: null; border-top-style: none; border-top-width: 0px; border-right-style: none; border-right-width: 0px; border-bottom-style: none; border-bottom-width: 0px; border-left-style: none; border-left-width: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; color: rgb(116, 51, 153)"><font color="rgb(116, 51, 153)" size="1">LXD 2.0: Debugging and contributing to LXD [12/12]</font></a>
      </li>
    </ul>
</div>
</div>
