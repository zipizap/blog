---
title: "Fluxcd v2 - manage k8s with gitops"
date: 2022-02-04


draft: false

categories:
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
#- cve
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
- bash
#- ansible
#- quickref
- k8s
- gitops
---
<div>
<div ID="freeplaneNode__ID_325453341" class=FreeplaneNodeGen>
    <p>
      I've been wanting to play more with gitops, and finally found just-enough time to understand a little bit about Fluxcd-v2 ;)<br><font size="1">But not enough time to make a better blog-post and a demo, so I'll just frenzy-write-down my impressions as this is better then nothing :)</font>
    </p>
    <p>
<br>
    </p>
    <p>
      The main gitops tool seems to be ArgoCD, however fluxcd-v2 is very simple and seems a good first-step to understand and focus concepts.
    </p>
    <p>
<br>
    </p>
    <p>
      Fluxcd actual version is Fluxcd-v2 - all documentation can be found in their site&nbsp; <a href="https://fluxcd.io/">https://fluxcd.io/</a>&nbsp;and deep-dive details in their github-repo
    </p>
    <p>
<br>
    </p>
    <p>
      So the idea here is that we have 1 git-repo of our own, in which we put the configuration that we want the cluster to have - in a declarative way, much like in &quot;terraform&quot;-style - and that configuration will be automatically picked-up by fluxcd and applied to the k8s-cluster. So changes made in the git-repo will be automatically &quot;reconciled&quot; with the cluster to match the git-repo declared config,<br><br>This &quot;reconciliation&quot; happens periodically when fluxcd *polls* the repo for new commits, and also if/when the git-repo fires a *webhook* towards the fluxcd-ingress-webhook receiver.<br><br>
    </p>
    <p>
      Once changes are detected on the git-repo, fluxcd will apply the git-config over the cluster (&quot;reconcile), effectively overriding the cluster-configuration with the git-configuration.
    </p>
    <p>
<br>
    </p>
    <p>
      If for any reason, the cluster config is changed (for example manually), then in the next polling interval the fluxcd will detect that the cluster-config differs from the intended git-config, which will trigger a &quot;reconciliation&quot; to reconfigure the cluster-config so it aligns with the git-config. This means that with gitops, changes should be made on the git-repo and the cluster should only be observed - and we should avoid making changes directly on the cluster-config (as the cluster config will eventually align back to the git-config, on the next &quot;reconciliation&quot; interval). For troubleshooting there is a way to &quot;suspend&quot; the &quot;reconciliation&quot; but that is not the normal way of working with gitops.<br><br><br>So, gitops: your git-repo is the source-of-truth of what you want in the cluster :)<br>
    </p>
    <p>
      By mediating all configuration-changes through git, we also gain version-control of all changes made in the cluster, with optional access-control via Pull-Requests and reviewers, and the rest of the git workflow :)<br><br><br><br>
    </p>
</div>
<div ID="freeplaneNode__ID_1941626585" class=FreeplaneNodeGen>
    <p>
      Up untill now I've presented Fluxcd in very simple terms, to resume the 
      main ideas.<br>In reality, you can use multiple git-repos (multiple 
      &quot;sources&quot;), and from each git-repo specify multiple &quot;directory-paths&quot; 
      where you want to &quot;watch&quot; for multiple changes.<br><br>
    </p>
    <p>
      In the git-repo you can put:<br>&nbsp;&nbsp;- <b>any manifest.yaml files</b>, 
      as you would with &quot;kubectl apply -f manifest.yaml&quot;<br>&nbsp;&nbsp;- <b>kustomize.yaml 
      </b>files, as you would with &quot;kubectl apply -k ./&quot;<br>&nbsp;&nbsp;- <b>encrypted 
      secrets</b>&nbsp;(secret is stored encrypted in git-repo, and inside cluster 
      is automatically decrypted and ready to use)
    </p>
    <p>
      and some other new fluxcd-CR :<br>&nbsp;- <b>HelmReleases, HelmRepository, 
      etc </b>-<b>&nbsp;</b>to deploy charts either from local-repo or 
      remote-repository<br>&nbsp;-&nbsp;<b>GitRepository </b>for additional repos<br>&nbsp;- 
      <b>Kustomization </b>to add &quot;watches&quot; on &quot;git-repos/dir-paths&quot;<b><br>&nbsp;
</b>    </p>
    <p>
      <b>
</b>    </p>
    <p>
      <b>&nbsp;</b>
    </p>
</div>
<div ID="freeplaneNode__ID_611459500" class=FreeplaneNodeGen>
    <p>
      To tell fluxcd to &quot;watch&quot; a certain &quot;git-repo&quot; in a specific 
      &quot;directory-path&quot;, you use some <b>new flux-CRDs</b>, which 
      include <b>Kustomization (kustomization-fluxcd)</b>, and <b>HelmRelease</b><br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
      <b><font color="#9999ff" size="3">Kustomization&nbsp;&nbsp;(kustomization-fluxcd :) )</font></b><font color="#9999ff" size="3"><br size="3" color="#9999ff"></font>
    </p>
    <p>
      <font color="#ffffff" size="2">&nbsp;&nbsp;apiVersion: kustomize.toolkit.fluxcd.io/v1beta2 </font>
    </p>
    <p>
      <font color="#ffffff" size="2">&nbsp;&nbsp;kind: Kustomization</font><font size="2"><br size="2">&nbsp; </font><br><font size="2">This 
      &quot;Kustomization&quot; fluxcd resource is different from the normal 
      &quot;kustomization kubectl apply -k&quot; that we are used to see. They both have 
      the same &quot;name&quot; (unfortunately confusing) but they are different and 
      used each for different effects.</font><br>I'll call 
      this&nbsp;&nbsp;&quot;kustomization-fluxcd&quot; and the other &quot;kustomization-k8s&quot;.<br><br>The 
      kustomization-fluxcd is used to indicate flux that a certain &quot;git-repo&quot; 
      in a certain &quot;dir-path&quot; should be &quot;watched&quot;- from that moment on fluxcd 
      will poll the contents in that &quot;dir-path&quot; and will process any 
      file-changes detected:<br>
    </p>
    <ul>
      <li>
        <font size="2">If the processed file is a </font><font color="#ffffff" size="2"><b>kustomize.yaml-k8s</b></font><font size="2">&nbsp;file 
        then:<br size="2">- <b>new kustomize.yaml-k8s&nbsp;created </b>in git, gets 
        deployed to cluster<br size="2">- <b>kustomize.yaml-k8s&nbsp;changed </b>in git, 
        gets changes also deployed into cluster<br size="2">- <b>existing 
        kustomize.yaml-k8s&nbsp;deleted </b>in git, then its resources are 
        **not deleted** from cluster and will stay lingering in the cluster 
        untill manually removed (hmm) If you ever want to delete a <b>kustomize.yaml-k8s</b>, 
        then first edit that file and leave it empty of any resouce (an empty 
        kustomize.yaml) and let the cluster reconcile that as it will 
        effectively delete all the old objects. And then, finally delete the 
        empty&nbsp; <b>kustomize.yaml-k8s</b>&nbsp;file from the git-repo.<br size="2"><br size="2"></font>
      </li>
      <li>
        <font size="2">If the processed file is any other</font><font color="#ffffff" size="2">&nbsp;<b>manifest.yaml</b></font><b><font size="2">&nbsp;</font></b><font size="2">then:<br size="2">- 
        <b>new manifest.yaml created </b>in git, gets deployed to cluster<br size="2">- <b>manifest.yaml 
        changed </b>in git, gets changes also deployed into cluster<br size="2">- <b>existing 
        manifest.yaml deleted </b>in git, gets its resources also deleted from 
        cluster<br size="2"></font>
      </li>
    </ul>
    <p>
<br>
    </p>
    <p>
      Also, kustomization-fluxcd allow you to specify dependencies&nbsp;from other 
      kustomzation-fluxcd or HelmRelease, so you can specify that it &quot;<b>dependsOn</b>&quot; 
      some-other-kustomization-fluxcd.<br>Ie, if kustomizationA-fluxcd contains 
      a &quot;dependsOn: kustomizationB-fluxcd&quot; then the kustomizationA-fluxcd will 
      only be deployed *after* the kustomizationB-fluxcd is correctly deployed 
      and READY<br><br>
    </p>
    <p>
      There are a lot more options, described here: <a href="https://fluxcd.io/docs/components/kustomize/kustomization/">https://fluxcd.io/docs/components/kustomize/kustomization/</a><br><br>In 
      short, think of a kustomization-fluxcd as a &quot;watcher&quot; over a 
      &quot;git-repo/dir-path&quot; that will reconcile it into the cluster.
    </p>
</div>
<div ID="freeplaneNode__ID_774959065" class=FreeplaneNodeGen>
    <p>
      <br>
      <br>
      <a href="https://fluxcd.io/docs/components/kustomize/kustomization/">https://fluxcd.io/docs/components/kustomize/kustomization/</a>
    </p>
</div>
<div ID="freeplaneNode__ID_623998420" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#999999">apiVersion: kustomize.toolkit.fluxcd.io/v1beta2</font>
    </p>
    <p>
      <font color="#999999">kind: </font><font color="#ffffff">Kustomization</font>
    </p>
    <p>
      <font color="#999999">metadata: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;name: apps </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;namespace: flux-system&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# namespace of 
      this Kustomization </font>
    </p>
    <p>
      <font color="#999999">spec: </font>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# &quot;suspend&quot; reconciliation controller-cluster (usefull when 
      troubleshooting) </font>
    </p>
    <p>
      &nbsp;&nbsp;suspend: false
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# reconcile periodically fetch-source--&gt;reconcile-with-cluster 
      (min&nbsp;&nbsp;60s)</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;interval: 10m0s</font>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# &quot;path&quot; of the &quot;sourceRef&quot; containing either: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;. kustomization.yaml (k8s-kustomization) </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;TIP: create kustomization.yaml for existing yaml files: 
      &quot;kustomize create --autodetect --recursive&quot; + &quot;kustomize build | kubeval 
      --ignore-missing-schemas&quot; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;NOTE: kustomization.yaml (k8s) is very usefull: it's the 
      easiest way to list de-list what manifests to execute. And can 
      namespace-restrict the manifests-deployment. Its a connecting piece!</font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;NOTE: dont delete kustomization.yaml (k8s) files - their 
      listed-objects are not deleted, they are left lingering in the cluster...</font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;. all yaml manifests-files and subdirs/ </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&gt;&gt; requires all found yaml files to be valid k8s-yaml manifests 
      (or add them to&nbsp;&lbrace;.sourceignore, subdir/.sourceignore&rbrace; files) </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;path: ./apps/staging</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;sourceRef: </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;kind: GitRepository </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;name: flux-system</font>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# &quot;prune&quot; aka &quot;garbage-collection&quot; </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# Prune =&nbsp;&nbsp;if resource created by this kustomization is latter 
      removed from kustomization, </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# it will also be removed (pruned) from the cluster </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;prune: true</font>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# During cluster-deployment, block-and-wait until all resources are 
      READY, or hit-timeout-and-set-ready-false (will retry after interval) </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# wait + timeout = verify all reconciled resources (like 
      healthcheck-everything), and ignore &quot;healtchecks&quot; if they exist </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;wait: true </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;timeout: 2m</font>
    </p>
    <p>
      <br>
      <font color="#999999">&nbsp;&nbsp;# [opt] Dependencies (either other Kustomization(s)(fluxcd), or 
      HelmRelease(s)) </font>
    </p>
    <p>
      &nbsp;&nbsp;dependsOn:
    </p>
    <p>
      &nbsp;&nbsp;&nbsp;&nbsp;- name: infrastructure
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;# [opt] &quot;targetNamespace&quot; override the ns of all the objects 
      reconciled by this kustomization </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;targetNamespace: ns-where-objects-are-deployed </font>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <font color="#999999"># -- other options -- </font>
    </p>
    <p>
      <font color="#999999"># decryption&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- the git-secrets-encrypted are decrypted 
      into cluster-secrets-decrypted </font>
    </p>
    <p>
      <font color="#999999"># patches </font>
    </p>
    <p>
      <font color="#999999"># ServiceAccountName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- for rbac'ing how 
      kustomizations/helmsReleases are deploying into cluster (ex: limit 
      certain source to only certain ns) </font>
    </p>
    <p>
      <font color="#999999"># KubeConfig&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- for multicluster deployments (1 single 
      fluxcd installation deploying to remote-cluster(s)) </font>
    </p>
    <p>
      <font color="#999999"># postBuild </font>
    </p>
    <p>
      <font color="#999999"># healthChecks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- only healthcheck white-list (wait+timeout 
      will healthcheck-everything!)</font><br>
    </p>
    <p>
<br>
    </p>
    <p>
      # kustomization watches both:
    </p>
    <p>
      #&nbsp;&nbsp;- plain-manifests.yaml files (*.yaml)
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ When new manifest.yaml is added into git, gets deployed to 
      cluster (reconciled). When existing manifest.yaml is deleted in git, its 
      deleted from cluster
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ renaming mymanifest.yaml --&gt; mymanifest.yaml.disabled has same 
      effect as deleting the file :)
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Symlinks are ignored
    </p>
    <p>
      #
    </p>
    <p>
      #&nbsp;&nbsp;- kustomization.yaml files, of native k8s 
      kustomize.config.k8s.io/v1beta1
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ When new kustomization.yaml is added into git, gets deployed to 
      cluster (reconciled).
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BUT when existing kustomization.yaml is deleted in git, in the 
      cluster the kustomization is deleted but the 
      other-kustomization-creted-objects are
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;not deleted they will stay lingering in cluster untill manually 
      deleted
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ When a kustomization.yaml file is present in a &quot;watched&quot; 
      subdir, it &quot;leads&quot; and all other *.yaml files are ignored. The 
      kustomization.yaml file is processed
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;
    </p>
    <p>
<br>
    </p>
    <p>
      # Use kustomization (fluxcd) for:
    </p>
    <p>
      # - specify source, branch, path to subdir, containing kustomize.yaml or 
      *.yaml, that will be loaded
    </p>
    <p>
      # - dependencies: indicate dependencies with other 
      kustomizations/HelmReleases
    </p>
    <p>
<br>
    </p>
    <p>
      # Use kustomization (k8s) for:
    </p>
    <p>
      # - reference other manifests (either kustmization/helmreleease or 
      simple manifests) in relative/absolute subdirs
    </p>
    <p>
      # - make overloads (sight)
    </p>
    <p>
<br>
    </p>
    <p>
      # === Org and call-chaining :) ===
    </p>
    <p>
      # kustomization-fluxcd, sets watch on gitrepo-subdir containing:
    </p>
    <p>
      #&nbsp;&nbsp;+-&gt; kustomization-k8s, that in turn restrict namespace and chain-call 
      any other yaml in this or other subdir
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;restrict &quot;namespace: only-this-ns&quot;
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- HelmRelease|KustomizationK8s|manifestYaml|any-CR/CRD/MANIFEST
    </p>
    <p>
      #
    </p>
    <p>
      # Its usefull to use kustomize-k8s to &quot;glue&quot; manifests together (and 
      easily remove them from the list which cleans them up)
    </p>
    <p>
      # Its more usefull to use kustomize-k8s than kustomize-fluxcd to &quot;glue&quot; 
      together manifests, because:
    </p>
    <p>
      #&nbsp;&nbsp;kustomize-fluxcd works well to indicate a dir to &quot;watch&quot;, including 
      all its .yaml and subdirs. However when a kustmize-k8s.yaml file gets 
      deleted its objects are not cleaned-up!
    </p>
    <p>
      #&nbsp;&nbsp;kustomize-k8s lets one specify a list of manifests.yaml and a 
      restriction-namespace, and when that list gets elements removed then 
      they will be correctly cleaned-up from the cluster (are cleaned-up!)
    </p>
    <p>
<br>
    </p>
    <p>
      #&nbsp;&nbsp;/!\ kustomize-controller can only revert changes made to fields it 
      manages. This means that &quot;kubectl edit&quot; changes will only be reverted if 
      those fields are present in git.
    </p>
    <p>
      #&nbsp;&nbsp;/!\ kustomize-controller seems to not pick up *symbolic-link* files 
      (replace symlinks with kustomize.yaml (k8s) referencing those 
      shared-files, or actual non-shared file-copies)
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1719911350" class=FreeplaneNodeGen>
<p></p>
<p></p>
<p>And here follows a quick-resume</p></div>
<div ID="freeplaneNode__ID_564466375" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#999999">apiVersion: kustomize.toolkit.fluxcd.io/v1beta2 </font>
    </p>
    <p>
      <font color="#999999">kind: </font><font color="#ffffff">Kustomization</font>
    </p>
    <p>
      <font color="#999999">metadata: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;name: apps </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;namespace: myns</font>
    </p>
    <p>
      <font color="#999999">spec: </font>
    </p>
    <p>
      <font color="#ffffff">#&nbsp;suspend: true </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;interval: 10m0s</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;path: ./apps/staging</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;sourceRef: </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;kind: GitRepository </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;&nbsp;&nbsp;name: flux-system</font>
    </p>
    <p>
      <br>
      <font color="#ffffff">&nbsp;&nbsp;prune: true</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;wait: true </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;timeout: 15m</font>
    </p>
    <p>
      <br>
      #&nbsp;dependsOn:
    </p>
    <p>
      #&nbsp;&nbsp;&nbsp;- name: infrastructure
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_500246058" class=FreeplaneNodeGen>
    <p>
      <b><font color="#9999ff" size="3">&nbsp; </font></b>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b><font color="#9999ff" size="3">HelmRelease&nbsp;</font></b><font color="#9999ff" size="3"><br size="3" color="#9999ff"></font>
    </p>
    <p>
      <font color="#ffffff" size="2">&nbsp;&nbsp;apiVersion: helm.toolkit.fluxcd.io/v2beta1 </font>
    </p>
    <p>
      <font color="#ffffff" size="2">&nbsp;&nbsp;kind: HelmRelease</font><font size="2"><br size="2">&nbsp; </font>
    </p>
    <p>
      <font size="2">This new fluxcd-CR indicates that we want to deploy a new helm-release 
      in the cluster. </font><br>It<font size="2">&nbsp;uses as &quot;source&quot;&nbsp;&nbsp;either a: </font>
    </p>
    <ul>
      <li>
        <font size="2">HelmRepository+HelmChart+version: a&nbsp;HelmRepository, from which it 
        will download a HelmChart of a specific *version*</font>
      </li>
      <li>
        <font size="2">GitRepository+dir-path-to-chart: </font>a GitRepository, from which it 
        will download a dir-path containing the helm-chart
      </li>
    </ul>
    <p>
      <br>
      <br>
      Then it will take &quot;values&quot; from a configmap/secret (or from yaml values 
      indicated inside the HelmRelease-file itself), and deploy into the 
      cluster the helm-release.<br><br>The HelmRelease also contains options to 
      indicate how to do the 1st-time-&quot;installation&quot; of the helm-release, and 
      how to do the 2nd-3rd-time-&quot;upgrade&quot; of the helm-release once it exists:
    </p>
    <ul>
      <li>
        The 1st time the HelmRelease is created, it will &quot;install&quot; the release<br>The 
        &quot;install&quot; will make an attempt and if it fails then make &quot;retries&quot; 
        (&quot;remediating&quot; the end of each failed-retry with an &quot;uninstall&quot;)<br>If 
        install-last-retry fails, then with the option &quot;remediateLastFailure: 
        true&quot; makes it cleanup the upgrade-failed-objects&nbsp;
      </li>
    </ul>
    <ul>
      <li>
        The 2nd,3rd,... times the HelmRelease is applied, it will &quot;upgrade&quot; 
        the release<br>The &quot;upgrade&quot; will make an attempt and if it fails then 
        make &quot;retries&quot; (&quot;remediating&quot; the end of each failed-retry with an 
        &quot;upgr-remediation-strategy&quot;)<br>If upgrade-last-retry fails, 
        then the option &quot;remediateLastFailure: true&quot; makes it cleanup the 
        upgrade-failed-objects&nbsp;
      </li>
    </ul>
    <p>
      <br>
      The HelmRelease can also contain &quot;<b>dependsOn</b>:&quot; with a list of 
      other HelmRelease(s), in which case this HelmRelease will only &quot;install&quot; 
      after the other &quot;dependsOn-helm-releases&quot; are installed and READY (or 
      will be left pending because of failed-dependencies). This &quot;dependsOn:&quot; 
      only applies to the 1st-time-install of the HelmRelease, and from then 
      on is ignored in posterior &quot;upgrades&quot; of the HelmRelease.
    </p>
    <p>
<br>
    </p>
</div>
<div ID="freeplaneNode__ID_24644043" class=FreeplaneNodeGen>
    <p>
      <a href="https://fluxcd.io/docs/components/source/helmrepositories/">https://fluxcd.io/docs/components/source/helmrepositories/</a>
    </p>
</div>
<div ID="freeplaneNode__ID_1710650475" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#999999">apiVersion: source.toolkit.fluxcd.io/v1beta1 </font>
    </p>
    <p>
      <font color="#999999">kind: </font><font color="#ffffff">HelmRepository</font>
    </p>
    <p>
      <font color="#999999">metadata: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;name: bitnami </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;namespace: myns</font>
    </p>
    <p>
      <font color="#999999">spec: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;</font><font color="#ffffff">interval: 30m</font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;</font><font color="#ffffff">url: https://charts.bitnami.com/bitnami</font>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1475481449" class=FreeplaneNodeGen>
    <p>
      <br>
      <br>
      <a href="https://fluxcd.io/docs/components/helm/helmreleases/">https://fluxcd.io/docs/components/helm/helmreleases/</a>
    </p>
</div>
<div ID="freeplaneNode__ID_918645727" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#999999">apiVersion: helm.toolkit.fluxcd.io/v2beta1 </font>
    </p>
    <p>
      <font color="#999999">kind: </font><font color="#ffffff">HelmRelease</font>
    </p>
    <p>
      <font color="#999999">metadata: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;name: nginx </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;namespace: myns</font>
    </p>
    <p>
      <font color="#999999">spec: </font>
    </p>
    <p>
      <font color="#ffffff">#&nbsp;suspend: true&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] </font>
    </p>
    <p>
      &nbsp;<font color="#ffffff">&nbsp;releaseName: nginx-ingress-controller&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] 
      default &quot;[TargetNamespace-]Name&quot;</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;interval: 10m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# reconcile 
      periodically helmRelease--&gt;reconcile-with-cluster (Ex: 1h, 10m)<br>&nbsp;&nbsp;timeout:&nbsp;&nbsp;15m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# 
      [opt] helm-deploy timeout, (default 5m0s)</font><br>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;# Values - use &quot;values:&quot; and/or &quot;valuesFrom:&quot; </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;# NOTE: updating &quot;values:&quot; triggers *immediate* reconcile of 
      HelmRelease (any change of helmRelease.spec content triggers immediate 
      reconciliation) </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;# NOTE: updating the configmap/secret of &quot;valuesFrom:&quot; doesnt 
      trigger *immediate* reconcile of HelmRelease, but will be reconciled on 
      next-interval-reconciliation </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;values:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# values directly in this 
      helmRelease </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;replicaCount: 2 </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;valuesFrom:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# valuesFrom list of 
      configmaps/secrets&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;- kind: ConfigMap </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;name: prod-env-values </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;valuesKey: values-prod.yaml&nbsp;&nbsp;&nbsp;&nbsp;# [opt] key where the 
      values.yaml-content or a specific value can be found. 
      Default&nbsp;&quot;values.yaml&quot; </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;- kind: Secret&nbsp; </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;name: prod-tls-values </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;optional: true&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] When true, this 
      valuesFrom-element is optional and may not exist (HelmRelease will not 
      fail if it doesnt find it) </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;valuesKey: crt </font>
    </p>
    <p>
      <font color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;targetPath: tls.crt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] YAML dot.notation.path 
      where value is merged into. When set, valuesKey is expected to be a 
      single flat value.<br color="#33ffcc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# Defaults to 
      None when omitted, which results in the values getting merged at the 
      root&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font>
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
      <br>
<br>
    </p>
    <p>
      <font color="#9999ff">&nbsp;&nbsp;chart:</font>
    </p>
    <p>
      <font color="#9999ff">&nbsp;&nbsp;&nbsp;&nbsp;spec: </font><br>
    </p>
    <p>
      <font color="#999999">#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sourceRef:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# 1 sourceRef, of either 
      GitRepository or HelmRepository (which supports chart-version)</font>
    </p>
    <p>
      <font color="#999999">#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kind: GitRepository&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# GitRepository </font>
    </p>
    <p>
      <font color="#999999">#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: mygitrepo </font>
    </p>
    <p>
      <font color="#999999">#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;namespace: flux-system </font>
    </p>
    <p>
      <font color="#999999">#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;chart: ./relPath/From/Root/Of/GitRepo/to/helmChartFolder</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sourceRef:</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kind: HelmRepository&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#&nbsp;HelmRepository</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: bitnami </font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;namespace: flux-system</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;chart: nginx-ingress-controller</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;version: &quot;5.6.14&quot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# only&nbsp;&nbsp;if 
      sourceRef==HelmRepository, ignored for GitRepos</font><br><br><font color="#ff9999">&nbsp;&nbsp;# The 1st time 
      the HelmRelease is created, it will &quot;install&quot; the release </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;# The &quot;install&quot; will make an attempt and if it fails then make 
      &quot;retries&quot; (&quot;remediating&quot; the end of each failed-retry with an 
      &quot;uninstall&quot;) </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;# If install-last-retry fails, then the option 
      &quot;remediateLastFailure: true&quot; makes it cleanup the upgrade-failed-objects</font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;install:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# holds the configuration for Helm 
      install actions </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;crds: Create&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] handle crds in 
      mychart/crds/*&nbsp;&nbsp;&nbsp;(Skip, Create [default], CreateReplace) </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;createNamespace: true&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] if targetNamespace is 
      specified and does not exist then create it </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;remediation:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] when install fails, how 
      to remediate. Defaults to not perform any action (cluster dirty?) </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retries: 0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] repeat &quot;retries&quot;-times 
      the cycle: install&gt;failure&gt;uninstall&nbsp;&nbsp;(default 0) </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remediateLastFailure: true&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] If last-retry-fails, 
      uninstall the release (true), or </font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#&nbsp;&nbsp;&nbsp;leave failed-objs in 
      cluster pending ?manual? cleanup (false). Default false (cluster dirty?) </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;# The 2nd,3rd,... times the HelmRelease is applied, it will 
      &quot;upgrade&quot; the release </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;# The &quot;upgrade&quot; will make an attempt and if it fails then make 
      &quot;retries&quot; (&quot;remediating&quot; the end of each failed-retry with an 
      &quot;upgr-remediation-strategy&quot;) </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;# If upgrade-last-retry fails, then the option 
      &quot;remediateLastFailure: true&quot; makes it cleanup the upgrade-failed-objects&nbsp;<br color="#ff99ff">&nbsp;&nbsp;upgrade:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# 
      holds the configuration for Helm upgrade actions </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;crds: Skip&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] handle crds in 
      mychart/crds/*&nbsp;(Skip [default], Create , CreateReplace) </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;cleanupOnFail:&nbsp;true&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] if upgrade fails, delete 
      new resources created during upgrade </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;remediation:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# when install fails, how to 
      remediate </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;strategy:&nbsp;rollback&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] 
      upgr-remediation-strategy: &quot;rollback&quot; or &quot;uninstall&quot; (default 
      &quot;rollback&quot;) </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;retries: 0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] repeat &quot;retries&quot;-times 
      the cycle: upgrade&gt;remediation-strategy&gt;uninstall&nbsp;&nbsp;(default 0) </font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remediateLastFailure: true&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] If last-retry-fails, 
      then apply &quot;upgr-remediation-strategy&quot; (defaul false - ?leave cluster 
      dirty?)</font>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;&nbsp;dependsOn:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# list of other existing HelmReleases 
      that must be install-Ready before this HelmRelease is installed. Only 
      for &quot;install&quot;, ignored for &quot;upgrade&quot;
    </p>
    <p>
      &nbsp;&nbsp;- name: someOtherHelmReleaseThatMustBeReadyBefore
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
      # --- other opts ---
    </p>
    <p>
      #&nbsp;&nbsp;rollback:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] holds the 
      configuration for Helm rollback actions (defaults ok)
    </p>
    <p>
      #&nbsp;&nbsp;uninstall:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] holds the 
      configuration for Helm uninstall actions (defaults ok)
    </p>
    <p>
      #&nbsp;&nbsp;storageNamespace &nbsp;<br>#&nbsp;&nbsp;<font color="#ffffff">targetNamespace: 
      ns-where-objects-are-deployed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] &quot;targetNamespace&quot; override's 
      the ns of all the deployed-objects. Defaults .metadata.namespace</font><br>#&nbsp;&nbsp;kubeConfig 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] for remote clusters<br>#&nbsp;&nbsp;serviceAccountName 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;# [opt] for RBAC-restrict deployments
    </p>
    <p>
      #&nbsp;&nbsp;test
    </p>
    <p>
      #&nbsp;&nbsp;postRenderers<br><br><br><br>&nbsp;<br>#&nbsp;Every &quot;interval&quot;-time 
      reconcile (HelmRelese&lt;--&gt;cluster), using info
    </p>
    <p>
      # from the *chartX* *versionX*, downloaded from *helmRepository*<br># and 
      applied with the *values*
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_511731437" class=FreeplaneNodeGen>
<p>And here follows a quick-resume</p></div>
<div ID="freeplaneNode__ID_1314100197" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      <font color="#999999">apiVersion: helm.toolkit.fluxcd.io/v2beta1 </font>
    </p>
    <p>
      <font color="#999999">kind: </font><font color="#ffffff">HelmRelease</font>
    </p>
    <p>
      <font color="#999999">metadata: </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;name: nginx </font>
    </p>
    <p>
      <font color="#999999">&nbsp;&nbsp;namespace: myns</font>
    </p>
    <p>
      <font color="#999999">spec: </font>
    </p>
    <p>
      <font color="#ffffff">#&nbsp;suspend: true</font>
    </p>
    <p>
      &nbsp;<font color="#ffffff">&nbsp;releaseName: nginx-ingress-controller</font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;interval: 10m<br></font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;timeout:&nbsp;&nbsp;15m</font>
    </p>
    <p>
      <br>
<br>
    </p>
    <p>
      <font color="#cc9900">&nbsp;&nbsp;values:</font>
    </p>
    <p>
      <font color="#cc9900">&nbsp;&nbsp;&nbsp;&nbsp;replicaCount: 2 </font>
    </p>
    <p>
      <br>
      <font color="#9999ff">&nbsp;&nbsp;chart:</font>
    </p>
    <p>
      <font color="#9999ff">&nbsp;&nbsp;&nbsp;&nbsp;spec: </font><br>
    </p>
    <p>
      <font color="#9999ff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;chart: </font><font color="#6633ff">nginx-ingress-controller</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sourceRef:</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kind: HelmRepository</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: bitnami </font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;namespace: flux-system</font>
    </p>
    <p>
      <font color="#ccccff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;version: &quot;5.6.14&quot;</font><br>&nbsp;<br>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;install:</font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;remediation:</font>
    </p>
    <p>
      <font color="#ff9999">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remediateLastFailure: true</font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;upgrade:</font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;cleanupOnFail:&nbsp;true</font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;remediation:</font>
    </p>
    <p>
      <font color="#ff99ff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;remediateLastFailure: true</font>
    </p>
    <p>
<br>
    </p>
    <p>
      #&nbsp;dependsOn:<br>
    </p>
    <p>
      #&nbsp;- name: someOtherHelmReleaseThatMustBeReadyBefore
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;<br>
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1869574788" class=FreeplaneNodeGen>
<p>NOTE:&nbsp;&nbsp;how to save values.yaml file and let kustomization create from it a configmap to be used by HelmRelease</p></div>
<div ID="freeplaneNode__ID_1536562146" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      cat &lt;&lt;EOF &gt;myValues.yaml
    </p>
    <p>
      FOO: Bar
    </p>
    <p>
      EOF
    </p>
    <p>
<br>
    </p>
    <p>
      cat &lt;&lt;EOF &gt;./kustomization.yaml
    </p>
    <p>
      apiVersion: kustomize.config.k8s.io/v1beta1
    </p>
    <p>
      kind: Kustomization
    </p>
    <p>
      <font color="#ffffff">configMapGenerator: </font>
    </p>
    <p>
      <font color="#ffffff">- name: example-configmap-1 </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;files: </font>
    </p>
    <p>
      <font color="#ffffff">&nbsp;&nbsp;- values.yaml=myValues.yaml</font>
    </p>
    <p>
      EOF
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1301586761" class=FreeplaneNodeGen>
    <p>
      <b><font color="#9999ff" size="3">&nbsp; </font></b>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b><font color="#9999ff" size="3">Kustomization.yaml (kustomization-k8s :) ) </font></b>
    </p>
    <p>
      <font color="#ffffff" size="2">&nbsp;&nbsp;apiVersion: kustomize.config.k8s.io/v1beta1 </font>
    </p>
    <p>
      <font color="#ffffff" size="2">&nbsp;&nbsp;kind: Kustomization</font>
    </p>
    <p>
      <font size="2">&nbsp; </font>
    </p>
    <p>
      This is the kustomization from &quot;kubectl apply -k&quot; that we already know. 
      Its not a new CRD, but its also used a lot to &quot;glue&quot; what files should 
      be &quot;watched&quot;<br>It is very usefull as a &quot;pivot&quot;, much like a 
      selector of what other files/paths (from same same git-repo) to include 
      in the &quot;watch&quot;. It makes it easy to control what other files or paths to 
      &quot;watch&quot;.<br>Do remember, as indicated before, that when you 
      delete a kustomization.yaml file from the git-repo, the child-objects in 
      the cluster are not deleted or removed... this came to me as a surprise, 
      but seems like its just the way it is. So think twice before deleting 
      kustomization.yaml files ;)
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;
    </p>
</div>
<div ID="freeplaneNode__ID_1588479327" class=FreeplaneNodeGen>
    <p>
      <b><font color="#ffffff" size="4">Installation and usage</font></b><br>
    </p>
    <p>
      Fluxcd is composed of kubernetes controllers, and their installation is 
      called &quot;bootstraping&quot; the cluster, which in effect deploys the 
      controllers into &quot;flux-system&quot; namespace.<br>Also during 
      bootstrap you need to provide a git-repo and its keys, and fluxcd will 
      write the controllers-yaml files in that git-repo. All info is simple 
      and clearly explained in <a href="https://fluxcd.io/docs/installation/">https://fluxcd.io/docs/installation/</a>
    </p>
    <p>
      <br>
      <font color="#cccccc" size="2">NOTE: fluxcd will store in a secret in the cluster the deployment-key 
      (or ssh-key) used to connect to the bootstrap git-repo and used to write 
      the bootstrap components into the git-repo. So fluxcd keeps a key with 
      write access to that git-repo. This is the &quot;standard&quot; way of installing 
      fluxcd, but I dont fully agree with it and would instead prefer for 
      fluxcd to have read-only permitions to the git-repo. It does seem 
      possible make an alternative bootstrap from a git-repo using read-only 
      keys (as far as I could find it's not explicitly documented but it is 
      mentioned in forums). That feels like the best way to go ;) </font>
    </p>
    <p>
      <br>
      <br>
      <br>
      There are also best-practices guides in the official docs, including one 
      which recommends different approaches about how to structure the 
      git-repos for different teams-organization (see <a href="https://fluxcd.io/docs/guides/repository-structure/">here</a>)
    </p>
    <p>
<br>
    </p>
    <p>
      I've studied through the &quot;monorepo&quot; approach - here is quick resume of 
      the bootstrap files, and the monorepo filestructure.
    </p>
    <p>
      &nbsp;
    </p>
</div>
<div ID="freeplaneNode__ID_971020386" class=FreeplaneNodeGen>
    <p>
      <font size="1">(open image in new tab to see it bigger)</font>
    </p>

{{< figure
    src="image8986.png"
    link="image8986.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_1033018223" class=FreeplaneNodeGen>
    <p>
      <br>
<br>
    </p>
    <p>
      Notice how the kustomization-fluxcd watches a particular directory, 
      where in turn there is a kustomization-k8s referencing which 
      shared-files/dirs (from other paths, not subdirs!) to include in the 
      &quot;watch&quot;
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;
    </p>
    <p>
      &nbsp;
    </p>
</div>
<div ID="freeplaneNode__ID_712541843" class=FreeplaneNodeGen>
    <p>
      <b><font color="#ffffff" size="4">Tips and Troubleshooting</font></b><br><br>
    </p>
    <p>
<br>
    </p>
    <p>
      Show state of all flux CR (like &quot;kubectl get all-flux-crd&quot; :) ) - made 
      it myself , veeery handy ;)
    </p>
    <p>
<br>
    </p>
</div>
<div ID="freeplaneNode__ID_902537094" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p># define shell-fuction (like a more-inteligent-alias :) )</p>
<p>kf() &lbrace; k get $(kubectl api-resources | grep -i flux | cut -d' ' -f1 | tr '\n' ',' | sed 's/,$//g') -A; &rbrace;</p>
<p></p>
<p># From now on in this shell, call kf whenever you want</p>
<p>kf</p>
<p></p>
<p># Or even better, loop-and-update it every 2sec ;)</p>
<p>while :;do sleep 2; kf &amp;&gt; /tmp/a; clear; date; cat /tmp/a ; done</p></div>
</div>
<div ID="freeplaneNode__ID_1903735219" class=FreeplaneNodeGen>
    <p>
      <font size="1">NOTE: I use kubecolor to pretty-print the output in colors</font>
    </p>

{{< figure
    src="png_6780134974131683810.png"
    link="png_6780134974131683810.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_289583300" class=FreeplaneNodeGen>
    <p>
      <br>
      <br>
      <br>
      <br>
      To troubleshoot when some object is not in READY state, get its Status 
      from yaml (or description), like so:<br>
    </p>
    <p>
<br>
    </p>
</div>
<div ID="freeplaneNode__ID_550174837" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      kubectl -n&nbsp;&nbsp;flux-system get kustomization.kustomize.toolkit.fluxcd.io/<font color="#ffffff">flux-system</font>&nbsp;
      -o yaml
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_247669882" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_5192033643679111731.png"
    link="png_5192033643679111731.png"
    width="600"
>}}
</div>
<div ID="freeplaneNode__ID_740398693" class=FreeplaneNodeGen>
    <p>
      &nbsp;<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <br>
      <br>
      Show flux logs in real-time <font size="1">(not that usefull in fact...)</font>
    </p>
</div>
<div ID="freeplaneNode__ID_1886958222" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      clear; flux logs --all-namespaces --since=2m --follow
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_447579577" class=FreeplaneNodeGen>
    <p>
      <br>
      <br>
      <br>
      Manually trigger reconcilication of kustomization called &quot;<b>flux-system</b>&quot; 
      (--with-source means to also reconcile its associated source)
    </p>
    <p>
      <font size="1">Use this to pick-up new commit in git-repo if you dont yet have 
      webhook-receiver setup</font>
    </p>
</div>
<div ID="freeplaneNode__ID_925228713" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left">    <p>
      flux reconcile kustomization <font color="#ffffff">flux-system</font>&nbsp;--with-source
    </p>
</div>
</div>
<div ID="freeplaneNode__ID_1287927555" class=FreeplaneNodeGen>
    <p>
      <br>
      <br>
      <br>
      Manually suspend/resume reconciliation of any fluxcd-CRD (ex: 
      Kustomization-fluxcd or HelmRelease)
    </p>
</div>
<div ID="freeplaneNode__ID_1751739176" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>flux suspend kustomization &lt;myKustomizationCrd&gt;</p></div>
</div>
<div ID="freeplaneNode__ID_954872109" class=FreeplaneNodeGen>
<div class=codeNodeWithFormatting style="font-family: Courier New; font-size: 9pt; color: #00ff00; background-color: #333333; text-align: left"><p>flux resume kustomization &lt;myKustomizationCrd&gt;</p></div>
</div>
<div ID="freeplaneNode__ID_1085333126" class=FreeplaneNodeGen>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
<br>
    </p>
    <p>
      <b><font color="#ffffff" size="4">Final thoughts</font></b><br><br><br>Fluxcd contains more features as sending 
      notifications (slack,teams,xmpp), receive webhooks (docker-registry, 
      git-repo).<br>And image-automation which is kind-of letting fluxcd detect 
      when there is a new image in docker-registry and then automatically 
      deploy the new image.<br><br><br>
    </p>
    <p>
<br>
    </p>
    <p>
      Wrapping the head around fluxcd the first time is a bit harder than it 
      initially looks like. However once the pieces are understood and put in 
      place, it feels very easy to use as it automates most of the management 
      of the cluster towards lower maintenance. Also its a welcome relief to 
      know that all the cluster config is stored and kept under 
      version-control, and the cluster-changes can be easily tracked-back and 
      reviewed. Or even the git-repo used to re-create an identical cluster 
      replica.
    </p>
    <p>
<br>
    </p>
    <p>
      &nbsp;
    </p>
</div>
</div>
