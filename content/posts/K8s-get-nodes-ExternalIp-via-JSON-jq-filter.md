---
title: "K8s get nodes ExternalIp via JSON jq filter"
date: 2020-02-26
draft: false

categories: 
#- Infosec
- Devops
- Sysadmin
- Cloud
#- Programming

tags:
- 4hackers
#- 4nonhackers
#- informative
#- exploitation
#- suid
#- etc
#- portugues
#- sociology
#- lxc
#- lxd
#- docker
#- bash
#- ansible
#- quickref
- k8s
- jq
---
<div>
<div ID="freeplaneNode__ID_1621307354" class=FreeplaneNodeGen>
    <p>
      Before I forget, here is a quick-and-dirty way of getting the 
      cluster-node <b>ExternalIp </b>addresses, via <b>JSON </b>and <b>jq filters</b>.<br>
    </p>
    <p>
<br>
    </p>
    <p>
      The interesting part is really how to do it using <a href="https://stedolan.github.io/jq/manual/#ConditionalsandComparisons">jq filters </a>- JSON 
      is everywhere so this should be helpfull in more than one situation<br>
    </p>
</div>
<div ID="freeplaneNode__ID_821396677" class=FreeplaneNodeGen>
<p></p>
{{< figure
    src="png_7920980979668963710.png"
    link="png_7920980979668963710.png"
    width="600"
>}}
</div>
</div>
