---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}

categories:
- Infosec
#- Devops
#- Sysadmin
#- Cloud
#- Programming

# tags will be Capitalized automatically
tags:
- exploitation
- suid
#- etc

draft: true
---

